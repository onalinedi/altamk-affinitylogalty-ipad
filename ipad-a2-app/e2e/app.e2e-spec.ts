import { IpadA2AppPage } from './app.po';

describe('ipad-a2-app App', () => {
  let page: IpadA2AppPage;

  beforeEach(() => {
    page = new IpadA2AppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
