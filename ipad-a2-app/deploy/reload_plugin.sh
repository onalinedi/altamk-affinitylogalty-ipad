#!/usr/bin/env bash

echo "Removing plugin"

cordova plugin remove com.altamk.cordova.plugins.logalty

echo "Installing plugin"

cordova plugin add local\ plugins/Logalty
