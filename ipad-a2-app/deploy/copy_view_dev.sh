#!/usr/bin/env bash

echo "Build preproduction"
cd ..
ng build -e pre
cd ..


echo "Removing cordova files and copying angular app"
echo "Removing regular dir"
rm -rf app/www/*
echo "Copying to regular dir"
cp -prf ipad-a2-app/dist/* app/www

echo "Copying to generated dir"
cp -prf ipad-a2-app/dist/* app/platforms/ios/Affinity\ Card.xcarchive/Products/Applications/Affinity\ Card.app/www


#cordova build
#cordova run browser
#cordova emulate ios --target="iPad-2"
