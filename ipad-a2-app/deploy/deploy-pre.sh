#!/usr/bin/env bash

echo "Build preproduction"
cd ..
ng build -e pre
cd ..


echo "Removing cordova files and copying angular app"
rm -rf app/www/*
cp -prf ipad-a2-app/dist/* app/www

cd app/www

echo "Updating index.html"
grep -v "<!--SecurityPolice>" index.html > tempIndex && mv tempIndex index.html
grep -v "<SecurityPolice-->" index.html > tempIndex && mv tempIndex index.html
grep -v " <!--RemoveInCordova-->" index.html > tempIndex && mv tempIndex index.html
sed -i .original 's/"\/"/".\/"/g' index.html
rm index.html.original

cd ..
rm icon.png
cp icon_dev.png icon.png
rm splash.png
cp splash_dev.png splash.png

#cordova-icon
#cordova-splash

echo "Building cordova"
cordova build
#cordova run browser
#cordova emulate ios --target="iPad-2"
