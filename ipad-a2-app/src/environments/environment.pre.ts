import {VERSION} from './version';

export const environment = {
  version:VERSION,
  production: false,
  serverBaseURL:"http://149.202.202.87:3000",
  cardCode:"affinity",
  autoLogin:false,
  loginInfo:{login:"",password:"",shop:""},
  debugMode:true,
  mode:"preproduction"
};
