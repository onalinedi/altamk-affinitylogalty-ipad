import {VERSION} from './version';

export const environment = {
  version:VERSION,
  production: true,
  serverBaseURL:"https://solicitudonline.es/altamkserver",
  cardCode:"affinitylogalty",
  loginInfo:{},
  mode:"production"
};
