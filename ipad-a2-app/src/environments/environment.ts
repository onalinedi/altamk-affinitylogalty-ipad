// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import {VERSION} from './version';

export const environment = {
  version:VERSION,
  production: false,
  serverBaseURL:"http://192.168.1.10:3000",
  //serverBaseURL:"http://localhost:3000",
  cardCode:"affinity",
  loginInfo:{login:"ivan.rivera",password:"altamk",shop:"test",card:"affinity"},
  autoLogin:true,
  debugMode:true,
  mode:"development"
};

// export const environment = {
//   version:VERSION,
//   production: false,
//   serverBaseURL:"http://149.202.202.87:3000",
//   cardCode:"affinity",
//   autoLogin:false,
//   loginInfo:{login:"",password:"",shop:""},
//   debugMode:true,
//   mode:"preproduction"
// };
