/**
 * Created by Ivan on 02/03/2017.
 */
"use strict";
var ErrorResponses = (function () {
    function ErrorResponses() {
    }
    ErrorResponses["NO_CURRENT_SHOP_FOUND"] = "shops-" + 404 /* NOT_FOUND */;
    ErrorResponses["NO_CURRENT_ACTION_FOUND"] = "actions-" + 404 /* NOT_FOUND */;
    ErrorResponses["NO_ACTION_FOUND"] = "actions-" + 404 /* NOT_FOUND */;
    ErrorResponses["NO_USER_FOUND"] = "users-" + 404 /* NOT_FOUND */;
    ErrorResponses["USER_UNAUTHORIZED"] = "users-" + 401 /* UNAUTHORIZED */;
    return ErrorResponses;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ErrorResponses;
