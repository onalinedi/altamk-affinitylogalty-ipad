import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

if (environment.production) {
  enableProdMode();
}
declare var runningInCordova: boolean;
var VERSION: String = "1.0";

platformBrowserDynamic().bootstrapModule(AppModule);

