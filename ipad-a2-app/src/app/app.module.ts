import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {WelcomeComponent} from './views/welcome/welcome.component';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';

import {ContextService} from './services/context.service';
import {StoreService} from './services/store.service';
import {ServerService} from './services/server.service';

import {ModalModule} from 'angular2-modal';
import {BootstrapModalModule} from 'angular2-modal/plugins/bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FormButtonComponent} from './components/form-button/form-button.component';
import {SidebarInfoComponent} from './components/sidebar-info/sidebar-info.component';

import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './views/page-not-found/page-not-found.component';

import {appRoutes} from './app.routes';
import {ProcessHeaderComponent} from './components/process-header/process-header.component';
import {FormProgressComponent} from './components/form-progress/form-progress.component';
import {NgbProgressbar, NgbProgressbarConfig, NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {FormHeaderComponent} from './components/form-header/form-header.component';
import {FormCheckButtonComponent} from './components/form-check-button/form-check-button.component';
import {IBANFormComponent} from './components/ibanform-component/ibanform-component.component';
import {FormErrorsPanelComponent} from './components/form-errors-panel/form-errors-panel.component';
import {ProfessionalDataComponent} from './components/professional-data/professional-data.component';
import {FinishFormComponent} from './views/finish-form/finish-form.component';
import {PdfViewerComponent} from 'ng2-pdf-viewer';
import {LoadingAnimateModule, LoadingAnimateService} from 'ng2-loading-animate';
import {FooterComponent} from './components/footer/footer.component';
import {FormPersonalesComponent} from './views/form-personales/form-personales.component';
import {FormContactoComponent} from './views/form-contacto/form-contacto.component';
import {FormCompleteComponent} from './views/form-complete/form-complete.component';
import {FormOtrosPersonalesComponent} from './views/form-otros-personales/form-otros-personales.component';
import {FormProfesionalesComponent} from './views/form-profesionales/form-profesionales.component';
import {FormBancariosComponent} from './views/form-bancarios/form-bancarios.component';
import {RequestsListComponent} from './views/requests-list/requests-list.component';
import {QuestionFormComponent, QuestionFormContext} from './components/question-form/question-form.component';
import {FormDocumentacionComponent} from './views/form-documentacion/form-documentacion.component';
import {FileUploadModule} from 'ng2-file-upload';
import {FormIncidenciasComponent} from "./views/form-incidencias/form-incidencias.component";
declare var jQuery: any;


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginFormComponent,
    SidebarComponent,
    FormButtonComponent,
    SidebarInfoComponent,
    PageNotFoundComponent,
    ProcessHeaderComponent,
    FormProgressComponent,
    FormHeaderComponent,
    FormCheckButtonComponent,
    IBANFormComponent,
    FormErrorsPanelComponent,
    ProfessionalDataComponent,
    FinishFormComponent,
    PdfViewerComponent,
    FooterComponent,
    FormPersonalesComponent,
    FormContactoComponent,
    FormCompleteComponent,
    FormOtrosPersonalesComponent,
    FormProfesionalesComponent,
    FormBancariosComponent,
    RequestsListComponent,
    QuestionFormComponent,
    FormDocumentacionComponent,
    FormIncidenciasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    RouterModule.forRoot(appRoutes),
    NgbProgressbarModule,
    LoadingAnimateModule.forRoot(),
    FileUploadModule
  ],
  providers: [
    StoreService,
    ContextService,
    ServerService,
    NgbProgressbarConfig,
    LoadingAnimateService
  ],
  bootstrap: [AppComponent],
  entryComponents: [LoginFormComponent, QuestionFormComponent]
})
export class AppModule {
}
