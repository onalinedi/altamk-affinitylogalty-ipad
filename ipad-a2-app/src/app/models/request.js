"use strict";
exports.RequestPrototype = {
  form: {
    personalForm: {
      username: "",
      surname1: "",
      surname2: "",
      idCode: "",
      idNumber: "",
      genre: "",
      birthDay: "",
      birthMonth: "",
      birthYear: "",
      birthCountry: "ESPAÑA",
      birthCity: "",
      nationality: "",
      workingStatus: "",
      parentWorkingStatus: "",
    },
    contactForm: {
      type: "",
      name: "",
      number: "",
      portal: "",
      block: "",
      stair: "",
      floor: "",
      door: "",
      rest: "",
      postalCode: "",
      city: "",
      province: "",
      mobilePhone: "",
      homePhone: "",
      email: ""
    },
    otherPersonalForm: {
      civilState: "",
      housingType: "",
      housingTypeOther: "",
      housingYears: "",
      personsResponsable: "",
    }
  }
};
exports.RequestPrototypeDebugForm = {
  form: {
    personalForm: {
      username: "Ivan",
      surname1: "Rivera",
      surname2: "",
      idCode: "DNI",
      idNumber: "33524764X",
      genre: "male",
      birthDay: "22",
      birthMonth: "1",
      birthYear: "1975",
      birthCountry: "ESPAÑA",
      birthCity: "Madrid",
      nationality: "Español",
      workingStatus: "Ama de casa",
      parentWorkingStatus: "Estudiante",
    },
    contactForm: {
      type: "",
      name: "",
      number: "",
      portal: "",
      block: "",
      stair: "",
      floor: "",
      door: "",
      rest: "",
      postalCode: "",
      city: "",
      province: "",
      mobilePhone: "",
      homePhone: "",
      email: ""
    },
    otherPersonalForm: {
      civilState: "casado",
      housingType: "propia",
      housingTypeOther: "none",
      housingYears: "10",
      personsResponsable: "1",
    }
  }
};
