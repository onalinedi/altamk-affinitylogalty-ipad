import {environment} from '../../environments/environment';

export const RequestPrototype = {
  id: "",
  userId: "",
  id_code: "",
  form: {
    personal: {
      username: "",
      surname1: "",
      surname2: "",
      idCode: "",
      idNumber: "",
      genre: "",
      birthDay: "",
      birthMonth: "",
      birthYear: "",
      birthCountry: "ESPAÑA",
      birthCity: "",
      nationality: "Española",

    },
    contact: {
      type: "Calle",
      name: "",
      number: "",
      portal: "",
      block: "",
      stair: "",
      floor: "",
      door: "",
      rest: "",
      postalCode: "",
      city: "",
      province: "",
      mobilePhone: "",
      homePhone: "",
      email: ""
    },
    otherPersonal: {
      civilState: "",
      housingType: "",
      housingTypeOther: "",
      housingYears: "",
      personsResponsable: "",
    },
    bank: {
      mensualPayment: "18,30 €",
      protectionPlan: "No",
      iban: "",
      disableAdvices: "",
      disableThirdPartyInfo: "",
      isPending:true
    },
    professional: {
      occupation: "Por cuenta ajena",
      parentOccupation: "",
      rentType: "",
      pensionType: "",
      companyName: "",
      activitySector: "",
      timeInEmployement: "",
      address: "",
      city: "",
      province: "",
      phone: "",
      activity: "",
      mensualIncomes: "",
      foreignType: "",
      isPending:true
    },
    pending:{
      bankAccount:{iban:null},
      professional:{
        companyName: null,
        activitySector: null,
        timeInEmployement: null,
        address: null,
        city: null,
        province: null,
        phone: null,
        activity: null,
        mensualIncomes: null,
        foreignType: null
      }
    }
  },
  documents:{},
  appVersion: environment.version
}

export const RequestPrototypeDebug = {
  id: "",
  userId: "",
  form: {
    personal: {
      username: "Joan",
      surname1: "Rovira",
      surname2: "Gonzalez",
      idCode: "NIF",
      idNumber: "74549257R",
      genre: "male",
      birthDay: "26",
      birthMonth: "5",
      birthYear: "1983",
      birthCountry: "ESPAÑA",
      birthCity: "Madrid",
      nationality: "Mexicano",
    },
    contact: {
      type: "Calle",
      name: "Sol",
      number: "12",
      portal: "",
      block: "",
      stair: "",
      floor: "",
      door: "",
      rest: "",
      postalCode: "28001",
      city: "Cogoyuyos",
      province: "Santa Cruz de Tenerife",
      mobilePhone: "619256001",
      homePhone: "916666666",
      email: "ivan.rivera@zonamultimedia.com"
    },
    otherPersonal: {
      civilState: "casado",
      housingType: "de los padres",
      housingTypeOther: "---",
      housingYears: "40",
      personsResponsable: "4",
    },
    bank: {
      mensualPayment: "60,10 €",
      protectionPlan: "Si",
      iban: "ES2773972556499892572640",
      disableAdvices: "",
      disableThirdPartyInfo: "",
      isPending:false
    },
    professional: {
      occupation: "Ama de casa",
      parentOccupation: "Por cuenta ajena",
      rentType: "Incapacidad permanente",
      pensionType: "Viudedad",
      companyName: "Nombre empresa",
      activitySector: "Servicios financieros",
      timeInEmployement: "3",
      address: "Avda de la empresa, 1",
      city: "Matalascañas",
      province: "Murcia",
      phone: "666666666",
      activity: "actividosa",
      mensualIncomes: "De 901€ a 1.200€",
      foreignType: "Fijo",
      isPending:false
    }
  },
  documents:{},
  appVersion: environment.version
}

export function checkFormDataValid(request) {
  var errors = [];
  if (!request['form']) {
    errors.push('No form defined');
  } else {
    var form = request['form'];
    Object.keys(RequestPrototype.form).forEach(function (property) {
      if (form[property] == undefined) {
        errors.push("No form root property '" + property + "' found");
      }
      else {
        Object.keys(RequestPrototype.form[property]).forEach(function (subProperty) {
          if (form[property][subProperty] == undefined) {
            errors.push("No form property '" + property + "." + subProperty + "' found");
          }
        })
      }
    });
  }
  if (errors.length > 0) {
    console.error(errors);
  }
  return errors;
}
export function parseRequestToView(serverRequestData) {
  return {
    id: serverRequestData.id,
    userId: serverRequestData.userId,
    id_code: serverRequestData.id_code,
    form: serverRequestData.form,
    documents:serverRequestData.documents
  }
}
