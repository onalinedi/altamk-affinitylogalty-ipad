import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDocumentacionComponent } from './form-documentacion.component';

describe('FormDocumentacionComponent', () => {
  let component: FormDocumentacionComponent;
  let fixture: ComponentFixture<FormDocumentacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDocumentacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDocumentacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
