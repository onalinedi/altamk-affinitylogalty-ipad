import {Component, OnInit, ElementRef, ViewChild, ApplicationRef, NgZone} from '@angular/core';
import {ContextService} from '../../services/context.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators, Form} from '@angular/forms';
import {StoreService} from '../../services/store.service';
import {FormAbstractComponent} from '../FormAbstract.component';
import {ServerService} from '../../services/server.service';
import {LoadingAnimateService} from 'ng2-loading-animate';
import {FileUploader} from 'ng2-file-upload';
import {ChangeDetectorRef} from '@angular/core';

declare var $: any;
declare var Camera: any;
declare var device: any;
declare var runningInCordova: any;
declare var window: any;
declare var navigator: any;

@Component({
  selector: 'app-form-documentacion',
  templateUrl: 'form-documentacion.component.html',
  styleUrls: ['form-documentacion.component.scss']
})
export class FormDocumentacionComponent extends FormAbstractComponent implements OnInit {

  isIpadMini: boolean;
  frontPictureTaken = false;
  backPictureTaken = false;
  documentPictureTaken = false;
  pictureToUploadSelector: string;
  public uploader: FileUploader;
  instance: any;
  frontPictureImageSrc = 'assets/img/dni_frontal.jpg';
  frontPictureErrors = [];
  backPictureImageSrc = 'assets/img/dni_trasera.jpg';
  backPictureErrors = [];
  documentPictureImageSrc = 'assets/img/document.jpg';
  documentPictureErrors = [];


  @ViewChild('formFileUploader') formFileUploader: ElementRef;

  constructor(private ref: ChangeDetectorRef, store: StoreService, context: ContextService, router: Router,
              fb: FormBuilder, server: ServerService, protected _loadingSvc: LoadingAnimateService,
              private _route: ActivatedRoute, protected ngZone: NgZone) {
    super(store, context, router, fb, server, _loadingSvc, ngZone);
    this.isIpadMini = this.context.isIpadMini();
    console.log('**** ' + (this.isIpadMini ? 'IS AN IPAD MINI' : 'IS NOT AN IPAD MINI') + ' *****');

    // Prepare image srcs
    if (this.store.documentFilePath('front') != null) {
      this.frontPictureImageSrc = this.server.getServerBasedFilesURL(
        this.store.documentFilePath('front')['url']) + '?nocache=' + Math.random();
      this.frontPictureErrors = this.store.currentRequest.documents.front.errors;
    }
    if (this.store.documentFilePath('back') != null) {
      this.backPictureImageSrc = this.server.getServerBasedFilesURL(this.store.documentFilePath('back')['url']) +
        '?nocache=' + Math.random();
      this.backPictureErrors = this.store.currentRequest.documents.back.errors;
    }
    if (this.store.documentFilePath('document') != null) {
      this.documentPictureImageSrc = this.server.getServerBasedFilesURL(this.store.documentFilePath(
        'document')['url']) + '?nocache=' + Math.random();
      this.documentPictureErrors = this.store.currentRequest.documents.document.errors;
    }

    this.setupUploader();
  }

  public ngOnInit() {
    window['logaltyPluginAccess'] = this.outsideFunc.bind(this);
  }

  private onSuccessUpload(itemresponse, status, headers, theStore, forceReload: boolean = false) {
    const theResult: any = JSON.parse(status);
    console.log('*** KEYS ' + JSON.stringify(Object.keys(theResult)));
    theStore.setCurrentRequest(theResult.result.request);
    console.log('*** SELECTOR ' + this.pictureToUploadSelector);
    switch (this.pictureToUploadSelector) {
      case 'front': {
        this.frontPictureTaken = true;
        this.frontPictureImageSrc = this.server.getServerBasedFilesURL(
          this.store.documentFilePath('front')['url']) + '?nocache=' + Math.random();
        this.frontPictureErrors = theResult.result.request.documents['front'].errors;
        console.log('*** FRONT DONE :' + this.frontPictureImageSrc);

        if (forceReload) {
          this.forceReloadPage();

        }

      }
        break;
      case 'back': {
        this.backPictureTaken = true;
        this.backPictureImageSrc = this.server.getServerBasedFilesURL(
          this.store.documentFilePath('back')['url']) + '?nocache=' + Math.random();
        this.backPictureErrors = theResult.result.request.documents['back'].errors;
        console.log('*** BACK DONE ' + this.backPictureImageSrc);
        if (forceReload) {
          this.forceReloadPage();

        }

      }
        break;
      case 'document': {
        this.documentPictureTaken = true;
        this.documentPictureImageSrc = this.server.getServerBasedFilesURL(
          this.store.documentFilePath('document')['url']) + '?nocache=' + Math.random();
        this.documentPictureErrors = theResult.result.request.documents['document'].errors;
        console.log('*** DOCUMENT DONE ' + this.documentPictureImageSrc);
        if (forceReload) {
          this.forceReloadPage();
        }

      }
        break;
    }

  }

  public forceReloadPage() {
    this._loadingSvc.setValue(true);
    console.log('*** FORCED RELOAD');
    if (this.router.url.indexOf('/documentacion2') === 0) {
      this.routeToGo = ['/documentacion', {refresh: +new Date()}];
    } else {
      this.routeToGo = ['/documentacion2', {refresh: +new Date()}];
    }
    this.saveAndSend();
  }

  private setupUploader() {
    this.uploader = new FileUploader({});
    this.uploader.onAfterAddingFile = (fileItem) => {
      this.uploader.clearQueue();
      this.uploader.queue[0] = fileItem;
      this._loadingSvc.setValue(true);
      this.server.uploadDocument(this.uploader, fileItem, this.pictureToUploadSelector, function (err, res) {
        this._loadingSvc.setValue(true);
      });
    };
    this.uploader.onErrorItem = (item, response, status, headers) => {

      this.server.commonRequestError({status: status, _body: response}, function (res) {
        this.context.modalDialog(
          'A ocurrido un error desconocido\nPor favor, ponga una incidencia describiendo el error',
          'Error');

      });
    };
    this.uploader.onSuccessItem = (itemresponse, status, headers) => {
      this.onSuccessUpload(itemresponse, status, headers, this.store, true);
    };
    this.uploader.onCompleteItem = (item, response, status, headers) => {
      item.isUploaded = false;
      this._loadingSvc.setValue(false);
      this.formFileUploader.nativeElement.value = null;
    };
  }

  protected saveAndSend() {
    this.saveRequestForm();
  }

  checkDocuments() {
    if (!this.store.currentRequest.documents || !this.store.currentRequest.documents.front ||
      !this.store.currentRequest.documents.back) {
      return -1;
    }
    const count = this.frontPictureErrors.length + this.backPictureErrors.length + this.documentPictureErrors.length;
    return count;
  }

  headerClicked(event) {
    switch (event) {
      case 'next':
        switch (this.checkDocuments()) {
          case -1:
            this.context.modalDialog(
              'Debe mandar al menos la captura frontal y trasera del documento de identidad', 'Error');
            break;
          case 0:
            this.routeToGo = '/pdf';

            this.saveAndSend();

            break;
          default:
            this.context.modalDialog(
              'No puede continuar mientras haya errores en los documentos almacenados\n Por favor, verifíquelos',
              'Error');
            break;
        }
        break;
      case 'prev' :
        this.routeToGo = '/bancarios';
        this.saveAndSend();
        break;

      default:
        this.context.modalDialog('form-documentacion.btnClicked evento desconocido: ' + event);
        return;

    }
  }

  takePicture(type) {
    this.pictureToUploadSelector = type;

    const isSimulator = this.context.isCordovaApp() && (device !== undefined) && device && device.model.match(/x86/);
    console.log('Is simulator' + isSimulator ? 'YES' : 'NO');
    if (this.context.isCordovaApp() && !isSimulator) {
      this.getCameraImage(type);
    } else {
      this.formFileUploader.nativeElement.click();
    }

  }

  public getCameraImage(imageType) {
    if (!navigator['camera'] === undefined || navigator['camera'] == null) {
      this.context.modalDialog('ERROR', 'No se ha podido detectar la cámara (1)');
      return;
    }
    if (!Camera) {
      this.context.modalDialog('ERROR', 'No se ha podido detectar la cámara (2)');
      return;
    }

    navigator['camera'].getPicture(
      // ImageSuccessCallback
      (imageURL) => {
        this._loadingSvc.setValue(true);
        // Get image handle
        this.server.uploadJpegFile(imageURL, imageType, (error, res) => {
          console.log('**** FILE UPLOADED');

          if (error) {
            console.error(error);
            this.context.modalDialog('Error en el envío (1)', error);
            return;
          }
          console.log('**** CALLING ONSUCCESS UPLOAD');

          this.onSuccessUpload(null, res.response, null, this.store, true);
        });
      },
      // ImageErrorCallback
      (message) => {
        console.log('ERROR: ' + message);
        if (message === 'no image selected') {
          return;
        }
        this.context.modalDialog(message, 'Error al obtener imagen');
      },
      // Options
      {
        quality: 50,
        destinationType: navigator['camera'].DestinationType.NATIVE_URI,
        sourceType: window.navigator['simulator'] === true ?
          navigator['camera'].PictureSourceType.PHOTOLIBRARY :
          navigator['camera'].PictureSourceType.CAMERA,
        allowEdit: false,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation: false,
        saveToPhotoAlbum: false,
        cameraDirection: navigator['camera'].Direction.BACK
      }
    )
    ;
  }


  public outsideFunc(command) {
    this.ngZone.run(() => this.privateOutsideFunc(command));
  }

  privateOutsideFunc(command) {
    console.log('Comando enviado a documentación:' + command);
    // this.context.modalDialog('Comando enviado a documentación:'+command,'Desde fuera');
  }
}
