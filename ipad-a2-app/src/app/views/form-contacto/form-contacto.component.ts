import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ContextService} from "../../services/context.service";
import {Router} from "@angular/router";
import Constants from "../../constants";
import {FormBuilder, FormGroup, FormControl, Validators, Form} from '@angular/forms';
import {StoreService} from "../../services/store.service";
import {IDCardValidatorGenerator} from "../../validators/IDCard.validator";
import {AgeValidatorGenerator} from "../../validators/Age.validator";
import {RequiredIfControlValueGenerator} from "../../validators/RequiredIfControlValue.validator";
import {EmailValidator} from "../../validators/Email.validator"
import {PhoneValidatorGenerator} from "../../validators/Phone.validator";
import {FormAbstractComponent} from "../FormAbstract.component";
import {ServerService} from "../../services/server.service";
import {LoadingAnimateService} from "ng2-loading-animate";
declare var $: any;

@Component({
  selector: 'app-form-contacto',
  templateUrl: 'form-contacto.component.html',
  styleUrls: ['form-contacto.component.scss']
})
export class FormContactoComponent extends FormAbstractComponent implements OnInit {

  //region Contact data values, attributes and validators
  days:string[]=[];
  years:string[]=[];
  contactDataForm: FormGroup
  contactDataErrors: string[] = [];

  addressTypeControl: FormControl;
  addressNameControl: FormControl;
  addressNumberControl: FormControl;
  addressPortalControl: FormControl;
  addressBlockControl: FormControl;
  addressStairControl: FormControl;
  addressFloorControl: FormControl;
  addressDoorControl: FormControl;
  addressRestControl: FormControl;
  addressPostalCodeControl: FormControl;
  addressCityControl: FormControl;
  addressProvinceControl: FormControl;
  mobilePhoneControl: FormControl;
  homePhoneControl: FormControl;
  contactEmailControl: FormControl;


  addressTypes: string[] = Constants.addressTypes;
  provinces: string[] = Constants.provinces;
  mobilePhoneValidator = new PhoneValidatorGenerator('mobile').validator();
  homePhoneValidator = new PhoneValidatorGenerator('home').validator();
  //endregion

  constructor(store: StoreService, context: ContextService, router: Router, fb: FormBuilder, server: ServerService,protected _loadingSvc: LoadingAnimateService,protected ngZone: NgZone) {
    super(store, context, router, fb, server,_loadingSvc,ngZone);
    for (var i = 1; i < 31; i++) {
      this.days.push("" + i);
    }
    var theDate = new Date();

    for (var i = theDate.getFullYear() - 18; i > theDate.getFullYear() - 100; i--) {
      this.years.push("" + i);
    }
    this.setupContactForm(this.fb);
  }

//region Contact form operations
  setupContactForm(fb: FormBuilder) {

    this.addressTypeControl = fb.control(this.store.getContactFormData('type'), [this.requiredAfterTrim]);
    this.addressNameControl = fb.control(this.store.getContactFormData('name'), [this.requiredAfterTrim]);
    this.addressNumberControl = fb.control(this.store.getContactFormData('number'), [this.requiredAfterTrim]);
    this.addressPortalControl = fb.control(this.store.getContactFormData('portal'), []);
    this.addressBlockControl = fb.control(this.store.getContactFormData('block'), []);
    this.addressStairControl = fb.control(this.store.getContactFormData('stair'), []);
    this.addressFloorControl = fb.control(this.store.getContactFormData('floor'), []);
    this.addressDoorControl = fb.control(this.store.getContactFormData('door'), []);
    this.addressRestControl = fb.control(this.store.getContactFormData('rest'), []);
    this.addressPostalCodeControl = fb.control(this.store.getContactFormData('postalCode'), Validators.compose([this.requiredAfterTrim, Validators.pattern(/^\d{5}$/)]));
    this.addressCityControl = fb.control(this.store.getContactFormData('city'), [this.requiredAfterTrim]);
    this.addressProvinceControl = fb.control(this.store.getContactFormData('province'), [this.requiredAfterTrim]);
    this.mobilePhoneControl = fb.control(this.store.getContactFormData('mobilePhone'), [this.requiredAfterTrim, this.mobilePhoneValidator]);
    this.homePhoneControl = fb.control(this.store.getContactFormData('homePhone'), [this.homePhoneValidator]);
    this.contactEmailControl = fb.control(this.store.getContactFormData('email'), Validators.compose([this.requiredAfterTrim, EmailValidator]));


    this.contactDataForm = fb.group({
      type: this.addressTypeControl,
      name: this.addressNameControl,
      number: this.addressNumberControl,
      portal: this.addressPortalControl,
      block: this.addressBlockControl,
      stair: this.addressStairControl,
      floor: this.addressFloorControl,
      door: this.addressDoorControl,
      rest: this.addressRestControl,
      postalCode: this.addressPostalCodeControl,
      city: this.addressCityControl,
      province: this.addressProvinceControl,
      mobilePhone: this.mobilePhoneControl,
      homePhone: this.homePhoneControl,
      email: this.contactEmailControl
    });


  }

  checkContactData() {
    var errors: string[] = [];

    this.checkAndAddError(this.addressTypeControl, 'required', 'Debe seleccionar un tipo de vía', errors);
    this.checkAndAddError(this.addressNameControl, 'required', 'Debe introducir el nombre de la vía', errors);
    this.checkAndAddError(this.addressNumberControl, 'required', 'Debe introducir el número de la vía', errors);
    this.checkAndAddError(this.addressPostalCodeControl, 'required', 'Debe introducir el código postal', errors);
    this.checkAndAddError(this.addressPostalCodeControl, 'pattern', 'Código postal incorrecto', errors);
    this.checkAndAddError(this.addressCityControl, 'required', 'Debe introducir la localidad', errors);
    this.checkAndAddError(this.addressProvinceControl, 'required', 'Debe seleccionar una provincia', errors);
    this.checkAndAddError(this.mobilePhoneControl, 'required', 'Debe introducit el teléfono móvil', errors);
    this.checkAndAddError(this.mobilePhoneControl, 'wrongNumber', 'Teléfono móvil incorrecto', errors);
    this.checkAndAddError(this.homePhoneControl, 'required', 'Debe introducit el teléfono fijo', errors);
    this.checkAndAddError(this.homePhoneControl, 'wrongNumber', 'Teléfono fijo incorrecto', errors);

    this.checkAndAddError(this.contactEmailControl, 'required', 'Debe introducir su dirección de email', errors);
    this.checkAndAddError(this.contactEmailControl, 'email', 'Email incorrecto', errors);

    this.contactDataErrors = this.removeDuplicates(errors);
    return this.contactDataErrors;
  }

  saveContactForm() {
    this.store.setContactFormData({
      type: this.addressTypeControl.value,
      name: this.addressNameControl.value,
      number: this.addressNumberControl.value,
      portal: this.addressPortalControl.value,
      block: this.addressBlockControl.value,
      stair: this.addressStairControl.value,
      floor: this.addressFloorControl.value,
      door: this.addressDoorControl.value,
      rest: this.addressRestControl.value,
      postalCode: this.addressPostalCodeControl.value,
      city: this.addressCityControl.value,
      province: this.addressProvinceControl.value,
      mobilePhone: this.mobilePhoneControl.value,
      homePhone: this.homePhoneControl.value,
      email: this.contactEmailControl.value
    })
  }

  //endregion

  protected saveAndSend(){
    this.saveContactForm();
    this.saveRequestForm();
  }

  headerClicked(event) {
    switch (event) {
      case "next":
        this.routeToGo = "/otros";
        this.checkContactData();
        if (this.contactDataErrors.length == 0) {
          this.saveAndSend()
        }
        break;

      case "prev":
        this.routeToGo = "/personales";
        this.saveAndSend();
        break;

      default:
        this.context.modalDialog("Form-contacto.btnClicked evento desconocido: " + event);
        return;

    }


  }

}
