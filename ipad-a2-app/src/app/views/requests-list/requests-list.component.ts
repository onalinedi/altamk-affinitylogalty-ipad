import {Component, OnInit, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LoadingAnimateService} from 'ng2-loading-animate';
import {ContextService} from '../../services/context.service';
import {StoreService} from '../../services/store.service';
import {ServerService} from '../../services/server.service';
import {parseRequestToView} from '../../models/request';

import {overlayConfigFactory, Modal} from 'angular2-modal';
import {QuestionFormComponent, QuestionFormContext} from '../../components/question-form/question-form.component';
import {BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {environment} from '../../../environments/environment';

declare var navigator: any;

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.scss']
})
export class RequestsListComponent implements OnInit {
  private sub: any;
  private statusToShow: string;
  private titleByStatus: string;
  private requests: any[];
  private hideTable = false;
  private environment: any = environment;

  constructor(private server: ServerService, private router: Router, private modal: Modal, private store: StoreService,
              private route: ActivatedRoute, protected _loadingSvc: LoadingAnimateService,
              protected context: ContextService, private zone: NgZone) {

  }

  ngOnInit() {
    this.loadData();
    this.context.setBodyClass('');
    if (this.store.isLogged() === false) {
      this.context.resetApp(true);
      //this.router.navigate(['/welcome']);
    }
    this.sub = this.route.params.subscribe(params => {
      this.statusToShow = params['status']; // (+) converts string 'id' to a number
      // console.info('RequestsListComponent: status to show = ' + this.statusToShow);
      if (this.statusToShow === 'DRAFT') {
        this.titleByStatus = 'Borradores';
      } else if (this.statusToShow === 'DONE_PENDING') {
        this.titleByStatus = 'Pendientes';
      } else {
        this.context.modalDialog('Estado no reconocido', 'Error', (res) => {
          this.router.navigate(['/']);
        });
      }
    });
  }

  private loadData() {
    this.hideTable = false;
    try {
      this.requests = this.store.getRequestsList();
    } catch (e) {
      console.log('Error getting requests list');
      console.log(e);
    }

    this._loadingSvc.setValue(false);

  }

  parseDate(theDate) {
    return new Date(theDate).toLocaleString();
  }

  goWelcome(event) {
    this.store.setRequestsList([]);

    this.context.resetApp(false);
    //this.router.navigate(['/']);
  }

  public openRequest(requestID, requestIdCode) {
    console.log('Open request ' + requestID);
    this.hideTable = true;

    const callback = (error, result) => {
      if (result != null) {
        if (result.toLowerCase() !== requestIdCode.toLowerCase()) {
          this.zone.run(() => {
            this._loadingSvc.setValue(false);
            this.hideTable = false;
            this.context.modalDialog('El código de identificación no coincide', 'Error', function (err, res) {

            });
          });
        } else {
          this._loadingSvc.setValue(true);
          this.server.getRequest(requestID, (requestResult) => {
            this._loadingSvc.setValue(false);
            this.store.setCurrentRequest(parseRequestToView(requestResult));
            if (this.statusToShow === 'DRAFT') {
              this.router.navigate(['/personales']);
            } else if (this.statusToShow === 'DONE_PENDING') {
              this.router.navigate(['/completar']);
            }
          }, (err) => {
            this.zone.run(() => {
              this._loadingSvc.setValue(false);
              this.hideTable = false;
            });
          });
        }
      } else {
        this.zone.run(() => {
          this._loadingSvc.setValue(false);
          this.hideTable = false;
        });
      }
    };

    const theContext = new QuestionFormContext();
    theContext.question = 'Introduzca el número de identificación del cliente';
    theContext.title = 'Atención';
    theContext.callback = callback;
    if (this.environment.mode === 'development') {
      theContext.response = requestIdCode.toLowerCase();
    }
    if (navigator && navigator['notification'] && navigator['notification']['confirm']) {
      const fn = navigator['notification']['prompt'];
      const nativeCallback = (results) => {
        this.hideTable = false;
        console.log(JSON.stringify(results));
        if (results.buttonIndex === 0 || results.buttonIndex === 2) {
          console.log('opening cancelled');
          callback(null, null);
        } else {
          console.log('opening with value ' + results.input1);
          callback(null, results.input1);
        }
      };
      fn(theContext.question, nativeCallback, theContext.title, ['OK', 'Cancelar']);
    } else {
      this.modal.open(QuestionFormComponent, overlayConfigFactory(theContext, BSModalContext));
    }
  }
}
