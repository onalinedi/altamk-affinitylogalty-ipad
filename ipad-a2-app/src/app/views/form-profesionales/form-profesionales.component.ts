import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ContextService} from "../../services/context.service";
import {Router} from "@angular/router";
import Constants from "../../constants";
import {FormBuilder, FormGroup, FormControl, Validators, Form} from '@angular/forms';
import {StoreService} from "../../services/store.service";
import {IDCardValidatorGenerator} from "../../validators/IDCard.validator";
import {AgeValidatorGenerator} from "../../validators/Age.validator";
import {RequiredIfControlValueGenerator} from "../../validators/RequiredIfControlValue.validator";
import {EmailValidator} from "../../validators/Email.validator"
import {PhoneValidatorGenerator} from "../../validators/Phone.validator";
import {FormAbstractComponent} from "../FormAbstract.component";
import {ServerService} from "../../services/server.service";
import {LoadingAnimateService} from "ng2-loading-animate";
import {ParentWorkingStatusValidatorGenerator} from "../../validators/ParentWorkingStatus.validator";

declare var $: any;


@Component({
  selector: 'app-form-profesionaless',
  templateUrl: 'form-profesionales.component.html',
  styleUrls: ['form-profesionales.component.scss']
})
export class FormProfesionalesComponent extends FormAbstractComponent implements OnInit {

  constants = Constants;
  //region professionalData form
  professionalDataForm: FormGroup;
  professionalDataErrors: string[] = [];
  parentWorkingStatusValidator;
  phoneValidator = new PhoneValidatorGenerator().validator();
  professionalTypeSelected: string;
  foreignTypeControl: FormControl;
  occupationControl: FormControl;
  parentOccupationControl: FormControl;
  mensualIncomesControl: FormControl;
  rentTypeControl: FormControl;
  pensionTypeControl: FormControl;
  activitySectorControl: FormControl;
  activityControl: FormControl;
  timeInEmployementControl: FormControl;
  addressControl: FormControl;
  companyNameControl: FormControl;
  cityControl: FormControl;
  provinceControl: FormControl;
  phoneControl: FormControl;
  activitySectors = Constants.activitySectors;
  timeInEmployement = Constants.timeInEmployement;
  provinces = Constants.provinces;
  mensualIncomes = Constants.mensualIncomes;

  errorsToPending: boolean = false;

  isHouseWife: boolean;
  //endregion
  constructor(store: StoreService, context: ContextService, router: Router, fb: FormBuilder, server: ServerService, protected _loadingSvc: LoadingAnimateService,protected ngZone: NgZone) {
    super(store, context, router, fb, server, _loadingSvc,ngZone);

    this.setupProfessionalDataForm(this.fb);


  }


//region Professional data operations
  setupProfessionalDataForm(fb: FormBuilder) {
    //Sets type
    //console.log("professionalTypeSelected:" + this.professionalTypeSelected);
    this.parentWorkingStatusValidator = new ParentWorkingStatusValidatorGenerator();

    this.occupationControl = fb.control(this.store.getProfessionalFormData('occupation'), [this.requiredAfterTrim]);
    this.parentOccupationControl = fb.control(this.store.getProfessionalFormData('parentOccupation'), [this.requiredAfterTrim, this.parentWorkingStatusValidator.validator()]);
    this.rentTypeControl = fb.control(this.store.getProfessionalFormData('rentType'), [this.requiredAfterTrim]);
    this.pensionTypeControl = fb.control(this.store.getProfessionalFormData('pensionType'), [this.requiredAfterTrim]);
    this.activitySectorControl = fb.control(this.store.getProfessionalFormData('activitySector'), [this.requiredAfterTrim]);
    this.activityControl = fb.control(this.store.getProfessionalFormData('activity'), [this.requiredAfterTrim]);
    this.timeInEmployementControl = fb.control(this.store.getProfessionalFormData('timeInEmployement'), [this.requiredAfterTrim, this.checkWorkingAgeCorrect()]);
    this.addressControl = fb.control(this.store.getProfessionalFormData('address'), []);
    this.cityControl = fb.control(this.store.getProfessionalFormData('city'), []);
    this.provinceControl = fb.control(this.store.getProfessionalFormData('province'), []);
    this.phoneControl = fb.control(this.store.getProfessionalFormData('phone'), [this.phoneValidator]);
    this.mensualIncomesControl = fb.control(this.store.getProfessionalFormData('mensualIncomes'), [this.requiredAfterTrim]);
    this.companyNameControl = fb.control(this.store.getProfessionalFormData('companyName'), [this.requiredAfterTrim]);
    this.foreignTypeControl = fb.control(this.store.getProfessionalFormData('foreignType'), [this.requiredAfterTrim]);

    this.parentWorkingStatusValidator.setControls(this.occupationControl, this.parentOccupationControl);

    this.professionalDataForm = fb.group({
      foreignType: this.foreignTypeControl,
      occupation: this.occupationControl,
      parentOccupation: this.parentOccupationControl,
      companyName: this.companyNameControl,
      rentType: this.rentTypeControl,
      pensionType: this.pensionTypeControl,
      activitySector: this.activitySectorControl,
      activity: this.activityControl,
      timeInEmployement: this.timeInEmployementControl,
      address: this.addressControl,
      city: this.cityControl,
      province: this.provinceControl,
      phone: this.phoneControl,
      mensualIncomes: this.mensualIncomesControl,
    });

    this.changeSelectedType();
  }

  checkProfessionalDataErrors(): string[] {
    var errors: string[] = [];

    this.checkAndAddError(this.occupationControl, 'required', 'Debe indicar la ocupación', errors);
    if (this.occupationControl.value == Constants.OCCUPATION_HOUSEWIFE) {
      this.checkAndAddError(this.parentOccupationControl, 'required', 'Debe indicar la ocupación del cónyuge', errors);
    }
    this.checkAndAddSpecialErrorC(this.parentOccupationControl, this.parentWorkingStatusValidator.validatorFn());
    this.checkAndAddError(this.mensualIncomesControl, 'required', 'Debe seleccionar los ingresos netos mensuales', errors);

    var lastErrors = errors.map(x => Object.assign({}, x));
    this.errorsToPending = false;

    switch (this.professionalTypeSelected) {
      case Constants.OCCUPATION_RENTIER:
        this.errorsToPending = this.checkAndAddError(this.rentTypeControl, 'required', 'Debe especificar el tipo de renta', errors) || this.errorsToPending;
        break;
      case Constants.OCCUPATION_PENSIONER:
        this.errorsToPending = this.checkAndAddError(this.pensionTypeControl, 'required', 'Debe especificar la causa de la pensión', errors) || this.errorsToPending;
        break;
      case Constants.OCCUPATION_UNEMPLOYED:
        break;
      case Constants.OCCUPATION_FOREIGN:
        this.errorsToPending = this.checkAndAddError(this.foreignTypeControl, 'required', 'Debe especificar el tipo de ocupación por cuenta ajena', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.companyNameControl, 'required', 'Debe especificar el nombre de la empresa/organismo', errors) || this.errorsToPending;

      case Constants.OCCUPATION_OWN:
        this.errorsToPending = this.checkAndAddError(this.activitySectorControl, 'required', 'Debe especificar el sector de actividad', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.activityControl, 'required', 'Debe especificar la actividad', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.timeInEmployementControl, 'required', 'Debe especificar la antigüedad', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.timeInEmployementControl, 'workingAge', 'Antigüedad incorrecta', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.phoneControl, 'wrongNumber', 'Número de teléfono incorrecto', errors) || this.errorsToPending;
        break;
    }
    if (this.occupationControl.value == Constants.OCCUPATION_HOUSEWIFE && this.errorsToPending == true && lastErrors.length == 0) {

    } else {
      this.errorsToPending = false;
    }
    console.dir(errors);
    this.professionalDataErrors = this.removeDuplicates(errors);
    return this.professionalDataErrors;
  }

  saveProfessionalData(isPending) {
    this.store.setProfessionalFormData({
      foreignType: this.foreignTypeControl.value,
      occupation: this.occupationControl.value,
      parentOccupation: this.parentOccupationControl.value,
      companyName: this.companyNameControl.value,
      rentType: this.rentTypeControl.value,
      pensionType: this.pensionTypeControl.value,
      activitySector: this.activitySectorControl.value,
      activity: this.activityControl.value,
      timeInEmployement: this.timeInEmployementControl.value,
      address: this.addressControl.value,
      city: this.cityControl.value,
      province: this.provinceControl.value,
      phone: this.phoneControl.value,
      mensualIncomes: this.mensualIncomesControl.value,
      isPending: isPending
    })
  }

  changeSelectedType() {
    this.professionalDataErrors = [];
    if (!this.occupationControl) {
      this.professionalTypeSelected = "";
      return;
    }
    if (this.occupationControl.value !== Constants.OCCUPATION_HOUSEWIFE) {
      this.professionalTypeSelected = this.occupationControl.value;
    } else {
      this.professionalTypeSelected = this.parentOccupationControl.value;
    }
  }

  //endregion
  checkWorkingAgeCorrect() {
    return (c: FormControl) => {
      var today = new Date();
      var birthDate = new Date(this.store.getPersonalFormData('birthYear') + "/" + this.store.getPersonalFormData('birthMonth') + "/" + this.store.getPersonalFormData('birthDay'));
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      //console.log("La edad es " + age + ", como mucho puede llevar trabajando " + (age - 16) + " años");
      var value = c.value;
      if (value == "Menos de un año") {
        value = "0";
      } else {
      }
      if (parseInt(value) <= age - 16) {
        return null
      }
      return {workingAge: true};
    };
  }

  protected saveAndSend(isPending:boolean) {
    this.saveProfessionalData(isPending);
    this.saveRequestForm();
  }

  headerClicked(event) {
     switch (event) {
      case "next":
        this.routeToGo = "/bancarios";
        this.checkProfessionalDataErrors();
        // SPECIAL FLOW
        if (this.professionalDataErrors.length == 0 || this.errorsToPending == true) {

          if (this.errorsToPending == true) {
            this.context.confirmDialog(
              "Al no especificar los datos de ocupación del cónyuge la solicitud se guardará como pendiente. ¿Desea continuar?",
              "Atención",
              ["Modificar", "Continuar"],
              (btnIndex) => {
                if (btnIndex == 2) {
                  this.saveAndSend(true)

                }
                return;
              }
            )
          } else {
            this.saveAndSend(false);
          }
        }
        break;

      case "prev":
        this.routeToGo = "/otros";
        this.saveAndSend(false)
        break;

      default:
        this.context.modalDialog("Form-profesionales.btnClicked evento desconocido: " + event);
        return;

    }


  }


}
