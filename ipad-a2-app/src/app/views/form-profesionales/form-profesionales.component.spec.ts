import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProfesionalesComponent } from './form-profesionales.component';

describe('FormProfesionalesComponent', () => {
  let component: FormProfesionalesComponent;
  let fixture: ComponentFixture<FormProfesionalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProfesionalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProfesionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
