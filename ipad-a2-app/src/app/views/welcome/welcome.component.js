"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var angular2_modal_1 = require('angular2-modal');
var bootstrap_1 = require('angular2-modal/plugins/bootstrap');
var login_form_component_1 = require('../login-form/login-form.component');
var environment_1 = require('../../environments/environment');
var request_1 = require("../models/request");
var WelcomeComponent = (function () {
    function WelcomeComponent(router, server, store, context, vcRef, modal) {
        this.router = router;
        this.server = server;
        this.store = store;
        this.context = context;
        this.modal = modal;
        this.showed = false;
        modal.overlay.defaultViewContainer = vcRef;
    }
    WelcomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedObserver = this.store.getLoggedObservable().subscribe(function (msg) {
            _this.logged = (msg == "logged");
            //console.log("Welcome.loggedObserver: User is " + msg);
        });
        //console.log("Welcome.ngInit: Subscribed to loggedObserver;");
        if (environment_1.environment.autoLogin && environment_1.environment.autoLogin == true && this.logged == false) {
            this.server.doUserLogin(environment_1.environment.loginInfo.login, environment_1.environment.loginInfo.password, environment_1.environment.loginInfo.shop, function (error, res) {
                //console.log("Autologin done");
            });
        }
    };
    WelcomeComponent.prototype.ngAfterViewChecked = function () {
    };
    WelcomeComponent.prototype.ngOnDestroy = function () {
        this.loggedObserver.unsubscribe();
        //console.log("Welcome.ngOnDestroy: Unsubscribed to loggedObserver;");
    };
    WelcomeComponent.prototype.doLogin = function () {
        this.closeSidebar();
        return this.modal.open(login_form_component_1.LoginFormComponent, angular2_modal_1.overlayConfigFactory(environment_1.environment.loginInfo, bootstrap_1.BSModalContext));
    };
    WelcomeComponent.prototype.closeSidebar = function () {
        if (this.sidebar.isVisible()) {
            this.sidebar.toggleMenu("out");
        }
    };
    WelcomeComponent.prototype.beginRequest = function (debug) {
        if (debug === void 0) { debug = false; }
        this.context.debugMode = debug;
        if (debug) {
            this.store.currentRequest = Object.assign({}, request_1.RequestPrototypeDebug);
        }
        this.closeSidebar();
        if (this.logged) {
            this.router.navigate(['/form1']);
        }
        else {
            this.doLogin();
        }
    };
    __decorate([
        core_1.ViewChild('sidebar')
    ], WelcomeComponent.prototype, "sidebar", void 0);
    WelcomeComponent = __decorate([
        core_1.Component({
            selector: 'app-welcome',
            templateUrl: 'welcome.component.html',
            styleUrls: ['welcome.component.scss']
        })
    ], WelcomeComponent);
    return WelcomeComponent;
}());
exports.WelcomeComponent = WelcomeComponent;
