import {Component, OnInit, AfterViewChecked, ViewContainerRef, ViewChild, NgZone} from '@angular/core';
import {ServerService} from '../../services/server.service';
import {Overlay, overlayConfigFactory} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {LoginFormContext, LoginFormComponent} from '../../components/login-form/login-form.component';
import {StoreService} from '../../services/store.service';
import {environment} from '../../../environments/environment';
import {ContextService} from '../../services/context.service';
import {Router} from '@angular/router';
import {RequestPrototype, RequestPrototypeDebug} from '../../models/request';

declare var Camera: any;
declare var cordova: any;
declare var LogaltyPlugin: any;
declare var navigator: any;

@Component({
  selector: 'app-welcome',
  templateUrl: 'welcome.component.html',
  styleUrls: ['welcome.component.scss']
})
export class WelcomeComponent implements OnInit, AfterViewChecked {
  showed = false;
  private loggedObserver: any;
  private logged: boolean;
  private environment: any = environment;

  @ViewChild('sidebar') sidebar;

  constructor(private router: Router, private server: ServerService, private store: StoreService,
              private context: ContextService, vcRef: ViewContainerRef, public modal: Modal, private ngZone: NgZone) {
    modal.overlay.defaultViewContainer = vcRef;

  }

  ngOnInit() {
    this.loggedObserver = this.store.getLoggedObservable().subscribe(msg => {
      this.logged = (msg === 'logged');
    });
    if (this.environment.autoLogin === true) {
      if (this.environment.autoLogin && this.environment.autoLogin === true && this.store.isLogged() === false &&
        this.environment['loginInfo'] !== undefined) {
        this.server.doUserLogin(environment.loginInfo.login, environment.loginInfo.password,
          environment.loginInfo.shop, function (error, res) {
        });
      }
    }
    console.log('******** Sidebar a out *************');
    this.sidebar.toggleMenu('out');
  }

  ngAfterViewChecked() {

  }

  ngOnDestroy() {
    this.loggedObserver.unsubscribe();
    // console.log('Welcome.ngOnDestroy: Unsubscribed to loggedObserver;')
  }

  protected doLogin() {
    this.closeSidebar();
    return this.modal.open(LoginFormComponent, overlayConfigFactory(environment.loginInfo, BSModalContext));
  }

  public closeSidebar() {
    if (this.sidebar.isVisible()) {
      this.sidebar.toggleMenu('out');
    }
  }


  protected beginRequest(debug: boolean = false) {
    this.context.debugMode = debug;
    if (debug) {
      this.store.currentRequest = Object.assign({}, RequestPrototypeDebug);
    } else {
      this.store.currentRequest = Object.assign({}, RequestPrototype);
    }
    this.closeSidebar();
    if (this.logged) {

      this.router.navigate(['/personales']);
    } else {
      this.doLogin();
    }
  }

  protected doCamera() {
    navigator['camera'].getPicture(
      // ImageSuccessCallback
      (imageURL) => {
        console.log('IMAGEURL: ' + imageURL);
      },
      // ImageErrorCallback
      (message) => {
        console.log('CAMERA ERROR: ' + message);
      },
      // Options
      {
        quality: 50,
        destinationType: navigator['camera'].DestinationType.NATIVE_URI,
        sourceType: navigator['camera'].PictureSourceType.CAMERA,
        allowEdit: false,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation: true,
        saveToPhotoAlbum: false,
        cameraDirection: navigator['camera'].Direction.BACK
      }
    )
    ;
  }
}

