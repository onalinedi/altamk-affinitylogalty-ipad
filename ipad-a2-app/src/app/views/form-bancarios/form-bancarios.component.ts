import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ContextService} from "../../services/context.service";
import {Router} from "@angular/router";
import Constants from "../../constants";
import {FormBuilder, FormGroup, FormControl, Validators, Form} from '@angular/forms';
import {StoreService} from "../../services/store.service";
import {IDCardValidatorGenerator} from "../../validators/IDCard.validator";
import {AgeValidatorGenerator} from "../../validators/Age.validator";
import {RequiredIfControlValueGenerator} from "../../validators/RequiredIfControlValue.validator";
import {EmailValidator} from "../../validators/Email.validator"
import {PhoneValidatorGenerator} from "../../validators/Phone.validator";
import {FormAbstractComponent} from "../FormAbstract.component";
import {ServerService} from "../../services/server.service";
import {LoadingAnimateService} from "ng2-loading-animate";
declare var $: any;
import {IBANValidatorGenerator} from "../../validators/IBAN.validator";
@Component({
  selector: 'app-form-bancarios',
  templateUrl: 'form-bancarios.component.html',
  styleUrls: ['form-bancarios.component.scss']
})
export class FormBancariosComponent extends FormAbstractComponent implements OnInit {


  //region bank data form
  bankDataForm: FormGroup;
  bankDataErrors: string[] = [];
  mensualPaymentForm: FormControl;
  protectionPlanForm: FormControl;
  ibanForm: FormControl;
  ibanFormValue: string;
  ibanValidator: IBANValidatorGenerator;
  disableThirdPartyInfoControl: FormControl;
  disableAdvicesControl: FormControl;
  //endregion


  constructor(store: StoreService, context: ContextService, router: Router, fb: FormBuilder, server: ServerService, protected _loadingSvc: LoadingAnimateService,protected ngZone: NgZone) {
    super(store, context, router, fb, server, _loadingSvc,ngZone);

    this.setupBankDataForm(this.fb);

  }

  //region Bank data operations
  setupBankDataForm(fb: FormBuilder) {
    this.ibanValidator = new IBANValidatorGenerator();

    this.mensualPaymentForm = fb.control(this.store.getBankFormData('mensualPayment'), [this.requiredAfterTrim]);
    this.protectionPlanForm = fb.control(this.store.getBankFormData('protectionPlan'), [this.requiredAfterTrim]);
    this.ibanForm = fb.control(this.store.getBankFormData('iban'), [this.ibanValidator.validator()]);
    this.ibanFormValue = this.store.getBankFormData('iban');
    this.disableThirdPartyInfoControl = fb.control(this.store.getBankFormData('disableThirdPartyInfo'));
    this.disableAdvicesControl = fb.control(this.store.getBankFormData('disableAdvices'));
    this.bankDataForm = fb.group({
      mensualPayment: this.mensualPaymentForm,
      protectionPlan: this.protectionPlanForm,
      iban: this.ibanForm,
      disableThirdPartyInfo: this.disableThirdPartyInfoControl,
      disableAdvices: this.disableAdvicesControl
    })
  }

  setIBANField(event) {
    this.ibanForm.setValue(event);
  }

  checkBankDataErrors(): string[] {
    var errors: string[] = [];
    this.checkAndAddError(this.mensualPaymentForm, 'required', 'Debe seleccionar la forma de pago de la tarjeta', errors);
    this.checkAndAddError(this.protectionPlanForm, 'required', 'Debe seleccionar si desea el plan de protección personal', errors);
    //this.checkAndAddError(this.ibanForm, 'iban', 'Código IBAN incorrecto', errors);
    this.bankDataErrors = this.removeDuplicates(errors);
    return this.bankDataErrors;
  }

  saveBankData(isPending:boolean) {
    this.store.setBankFormData({
      mensualPayment: this.mensualPaymentForm.value,
      protectionPlan: this.protectionPlanForm.value,
      iban: this.ibanForm.value,
      disableAdvices: this.disableAdvicesControl.value,
      disableThirdPartyInfo: this.disableThirdPartyInfoControl.value,
      isPending:isPending
    })
  }

  //endregion

  protected saveAndSend(isPending:boolean) {
    this.saveBankData(isPending);
    this.saveRequestForm();
  }

  internalConfirm(isPending:boolean){
    this.context.confirmDialog("¿Ha verificado que los datos del formulario son correctos?", "Atención", ["Modificar", "Son correctos"], (butIndex) => {
      if (butIndex == 2) {
        this.context.confirmDialog("Verifique el teléfono:\n"+this.store.getContactFormData('mobilePhone'), "Atención", ["Modificar", "Es correcto"], (butIndex) => {
          if (butIndex == 2) {
            this.saveAndSend(isPending);
          }
        });
      }
    });
  }
  headerClicked(event) {
    var doSaveAndSend=false;

    switch (event) {
      case "next":
        this.routeToGo = "/documentacion";
        this.checkBankDataErrors();
        if (this.bankDataErrors.length == 0) {
          // Check if IBAN is correct, else show message
          this.ibanForm.markAsDirty();
          var hasError = false;
          var message = "";
          if (this.ibanForm.hasError('iban')) {
            hasError = true;
            message = "El código IBAN introducido es incorrecto.\n Si continua se marcará la solicitud como pendiente.\n¿Desea continuar?";
          } else {

            if (this.ibanForm.value.trim() == "") {
              hasError = true;
              message = "No ha introducido el código IBAN.\n  Si continua se marcará la solicitud como pendiente.\n¿Desea continuar?"
            }
          }

          if (hasError) {
            this.context.confirmDialog(message, "Error en IBAN", ["Cancelar", "Continuar"], (butIndex) => {
              if (butIndex == 2) {
                this.internalConfirm(true);
              }
            })
          } else {
            this.internalConfirm(false);
          }


        }
        break;
      case
      "prev"
      :
        this.routeToGo = "/profesionales";
        this.saveAndSend(false);
        break;

      default:
        this.context.modalDialog("Form-bancarios.btnClicked evento desconocido: " + event);
        return;

    }


  }


}
