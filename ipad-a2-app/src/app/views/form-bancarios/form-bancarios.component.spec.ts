import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBancariosComponent } from './form-bancarios.component';

describe('FormBancariosComponent', () => {
  let component: FormBancariosComponent;
  let fixture: ComponentFixture<FormBancariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBancariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBancariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
