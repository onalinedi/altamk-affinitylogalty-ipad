import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormOtrosPersonalesComponent } from './form-otros-personales.component';

describe('FormOtrosPersonalesComponent', () => {
  let component: FormOtrosPersonalesComponent;
  let fixture: ComponentFixture<FormOtrosPersonalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormOtrosPersonalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormOtrosPersonalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
