import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ContextService} from "../../services/context.service";
import {Router} from "@angular/router";
import Constants from "../../constants";
import {FormBuilder, FormGroup, FormControl, Validators, Form} from '@angular/forms';
import {StoreService} from "../../services/store.service";
import {IDCardValidatorGenerator} from "../../validators/IDCard.validator";
import {AgeValidatorGenerator} from "../../validators/Age.validator";
import {RequiredIfControlValueGenerator} from "../../validators/RequiredIfControlValue.validator";
import {EmailValidator} from "../../validators/Email.validator"
import {PhoneValidatorGenerator} from "../../validators/Phone.validator";
import {FormAbstractComponent} from "../FormAbstract.component";
import {ServerService} from "../../services/server.service";
import {LoadingAnimateService} from "ng2-loading-animate";
declare var $: any;

@Component({
  selector: 'app-form-otros-personales',
  templateUrl: 'form-otros-personales.component.html',
  styleUrls: ['form-otros-personales.component.scss']
})
export class FormOtrosPersonalesComponent extends FormAbstractComponent implements OnInit {

  //region other personal data
  otherPersonalDataForm: FormGroup;
  otherPersonalDataErrors: string[] = [];
  housingYears = Constants.housingYears;
  personsResponsable = Constants.personsResponsable;
  requiredIfHouseTypeValidatorGenerator:RequiredIfControlValueGenerator;

  civilStateForm: FormControl;
  housingTypeForm: FormControl;
  housingTypeOtherForm: FormControl;
  housingYearForm: FormControl;
  personsResponsableForm: FormControl;
  //endregion

  constructor(store: StoreService, context: ContextService, router: Router, fb: FormBuilder, server: ServerService,protected _loadingSvc: LoadingAnimateService,protected ngZone: NgZone) {
    super(store, context, router, fb, server,_loadingSvc,ngZone);

    this.setupOtherPersonalDataForm(this.fb);

  }

protected saveAndSend(){
  this.saveOtherPersonalData();
  this.saveRequestForm();
}
  headerClicked(event) {
    switch (event) {
      case "next":
        this.routeToGo = "/profesionales";
        this.checkOtherPersonalData();
        if (this.otherPersonalDataErrors.length == 0) {
          this.saveAndSend();
        }
        break;

      case "prev":
        this.routeToGo = "/contacto";
        this.saveAndSend();
        break;

      default:
        this.context.modalDialog("Form-otros-personales.btnClicked evento desconocido: " + event);
        return;

    }


  }

  //region Other personal data operations
  setupOtherPersonalDataForm(fb: FormBuilder) {
    this.civilStateForm = fb.control(this.store.getOtherPersonalFormData('civilState'), [this.requiredAfterTrim]);
    this.housingTypeForm = fb.control(this.store.getOtherPersonalFormData('housingType'), [this.requiredAfterTrim]);
    this.requiredIfHouseTypeValidatorGenerator=new RequiredIfControlValueGenerator("requiredIf");
    this.housingTypeOtherForm = fb.control(this.store.getOtherPersonalFormData('housingTypeOther'), [this.requiredIfHouseTypeValidatorGenerator.validator()]);
    this.housingYearForm = fb.control(this.store.getOtherPersonalFormData('housingYears'), [this.requiredAfterTrim]);
    this.personsResponsableForm = fb.control(this.store.getOtherPersonalFormData('personsResponsable'), [this.requiredAfterTrim]);

    this.requiredIfHouseTypeValidatorGenerator.setValueControl(this.housingTypeForm,"otras");

    this.otherPersonalDataForm = fb.group({
      civilState: this.civilStateForm,
      housingType: this.housingTypeForm,
      housingTypeOther: this.housingTypeOtherForm,
      housingYears: this.housingYearForm,
      personsResponsable: this.personsResponsableForm
    })
  }
  checkOtherPersonalData(): string[] {
    this.housingTypeOtherForm.updateValueAndValidity();
    var errors: string[] = [];
    this.checkAndAddError(this.civilStateForm, 'required', 'Debe seleccionar un estado civil', errors);
    this.checkAndAddError(this.housingTypeForm, 'required', 'Debe seleccionar un tipo de vivienda', errors);
    this.checkAndAddError(this.housingYearForm, 'required', 'Debe seleccionar los años en la vivienda', errors);
    this.checkAndAddError(this.housingTypeOtherForm, 'requiredIf', 'Debe especificar el otro tipo de vivienda', errors);
    this.checkAndAddError(this.personsResponsableForm, 'required', 'Debe seleccionar un el número de personas a cargo', errors);
    this.otherPersonalDataErrors = this.removeDuplicates(errors);
    return this.otherPersonalDataErrors;
  }
  saveOtherPersonalData() {
    this.store.setOtherPersonalFormData({
      civilState: this.civilStateForm.value,
      housingType: this.housingTypeForm.value,
      housingTypeOther: this.housingTypeOtherForm.value,
      housingYears: this.housingYearForm.value,
      personsResponsable: this.personsResponsableForm.value
    })
  }

  //endregion

}
