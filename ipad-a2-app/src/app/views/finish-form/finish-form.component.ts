import {ChangeDetectorRef, Component, NgZone, OnInit} from '@angular/core';
import {StoreService} from "../../services/store.service";
import {Router} from "@angular/router";
import {FormAbstractComponent} from "../FormAbstract.component";
import {ContextService} from "../../services/context.service";
import {FormBuilder} from "@angular/forms";
import {ServerService} from "../../services/server.service";
import {environment} from "../../../environments/environment";
import {LoadingAnimateService} from "ng2-loading-animate";
declare var window: any;
declare var LogaltyPlugin: any;
@Component({
  selector: 'app-finish-form',
  templateUrl: 'finish-form.component.html',
  styleUrls: ['finish-form.component.scss']
})
export class FinishFormComponent extends FormAbstractComponent implements OnInit {

  showPdf: boolean = false;
  url: string = "";
  page = 0;
  zoom = 1.13;
  loadComplete = false;
  loading = true;

  constructor(store: StoreService, context: ContextService, router: Router, fb: FormBuilder, server: ServerService, protected _loadingSvc: LoadingAnimateService, protected ngZone: NgZone,private ref:ChangeDetectorRef) {
    super(store, context, router, fb, server, _loadingSvc, ngZone);
  }

  setLoadComplete(evt) {
    this.loadComplete = true;
  }

  isBrowser() {
    return !this.context.isCordovaApp() && this.loading == false;
  }

  ngOnInit() {
    this.loading = true;
    super.ngOnInit();
    this.context.setBodyClass("backgrounGray");
    window["logaltyPluginAccess"] = this.outsideFunc.bind(this);

    // TRY TO CHANGE REQUEST STATUS TO Constants.request.STATUS_LOGALTY_PENDING
    this.server.prepareRequestToLogalty((res) => {

      this.server.beginLogaltyProcess((res) => {
        if (this.context.isCordovaApp()) {
          this.loading = false;
          LogaltyPlugin.showLogaltyView(res.logalty.client_saml_url, res.logalty.client_saml_url,
            function (res) {
              console.log("**** LOGALTYPLUGIN_RES_OK");

            }, function (err) {
              console.log("**** LOGALTYPLUGIN_RES_ERROR");

              console.log(err);
            });
        } else {
          this.server.getRequestFormPdfURL(
            (res) => {
              this.loading = false;
              var url = environment.serverBaseURL + res.url;
              console.log(url);
              this.url = url;
              this.showPdf = true;
            },
            (error) => {
              this.context.modalDialog("Ha ocurrido un error obteniendo la URL del archivo PDF:" + error, "Error");
            }
          )
        }

      }, (err) => {
        err = err.parse()
        this.context.modalDialog("Ha ocurrido un error al comenzar el proceso de firma", "Error");
      })

    }, (err) => {
      err = err.parse()
      this.context.modalDialog("Ha ocurrido un error al generar la solicitud para su firma", "Error");
    })
  }

  protected saveAndSend() {
    // Save status to DRAFT
    this.server.changeRequestStatus("DRAFT", (result) => {
      this.saveRequestForm();
    }, (err) => {
    })
  }

  public isPending(): string {
    return this.store.isPending() ? "PENDIENTE" : "EXPRESS";
  }

  headerClicked(event) {
    switch (event) {
      case "prev":
        this.routeToGo = '/documentacion';
        this.router.navigate([this.routeToGo]);
        break;
      default:
        this.context.modalDialog("Form1.btnClicked evento desconocido: " + event);
        return;
    }
  }

  public outsideFunc(command) {
    this.ngZone.run(() => this.privateOutsideFunc(command));
  }

  public isLoading() {
    return this.loading;
  }

  privateOutsideFunc(command) {
    this.loading = false;
    switch (command) {
      case "completedL":
        this.context.resetApp(false);

//        this.router.navigate(["/welcome"]);
        break;
      case "cancelL":
        var eso=(resultOrError)=>{

            console.log("Cambiando a welcome");
           // this.router.navigate(["/welcome"]);
          this.context.resetApp(false);


        }
        console.log("---- Petición de cancelación de request Logalty");
        this.server.cancelLogaltyRequest(eso,eso);

        break;
      case "backL":

        var eso=(resultOrError)=>{

            console.log("llamando a evento Prev");
            this.headerClicked("prev");

        }
        console.log("---- Petición de volver a documentación de request Logalty");
        this.server.cancelLogaltyRequest(eso,eso);
        break;
      case "retryL":
        var eso=(resultOrError)=>{
            console.log("llamando a ngOnInit");
            this.ngOnInit();

        }
        console.log("---- Petición de reiniciar request Logalty");
        this.server.cancelLogaltyRequest(eso,eso);
        break;
      default:
        this.context.modalDialog("Recibido comando desconocido:" + command);
    }

  }
}
