import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ContextService} from '../../services/context.service';
import {Router} from '@angular/router';
import Constants from '../../constants';
import {FormBuilder, FormGroup, FormControl, Validators, Form} from '@angular/forms';
import {StoreService} from '../../services/store.service';

import {PhoneValidatorGenerator} from '../../validators/Phone.validator';
import {FormAbstractComponent} from '../FormAbstract.component';
import {ServerService} from '../../services/server.service';
import {LoadingAnimateService} from 'ng2-loading-animate';

declare var $: any;
import {IBANValidatorGenerator} from '../../validators/IBAN.validator';

@Component({
  selector: 'app-form-complete',
  templateUrl: 'form-complete.component.html',
  styleUrls: ['form-complete.component.scss']
})
export class FormCompleteComponent extends FormAbstractComponent implements OnInit {

  constants = Constants;
  completeForm: FormGroup;
  formErrors: string[] = [];
  isBankDataPending = false;
  isProfessionalPending = false;

  ibanForm: FormControl;
  ibanFormValue: string;
  ibanValidator: IBANValidatorGenerator;

  professionalDataForm: FormGroup;
  professionalDataErrors: string[] = [];
  parentWorkingStatusValidator;
  phoneValidator = new PhoneValidatorGenerator().validator();
  professionalTypeSelected: string;
  foreignTypeControl: FormControl;
  occupationControl: FormControl;
  parentOccupationControl: FormControl;
  mensualIncomesControl: FormControl;
  rentTypeControl: FormControl;
  pensionTypeControl: FormControl;
  activitySectorControl: FormControl;
  activityControl: FormControl;
  timeInEmployementControl: FormControl;
  addressControl: FormControl;
  companyNameControl: FormControl;
  cityControl: FormControl;
  provinceControl: FormControl;
  phoneControl: FormControl;
  activitySectors = Constants.activitySectors;
  timeInEmployement = Constants.timeInEmployement;
  provinces = Constants.provinces;
  mensualIncomes = Constants.mensualIncomes;
  errorsToPending = false;

  isHouseWife: boolean;

  constructor(store: StoreService, context: ContextService, router: Router, fb: FormBuilder, server: ServerService,
              protected _loadingSvc: LoadingAnimateService, protected ngZone: NgZone) {
    super(store, context, router, fb, server, _loadingSvc, ngZone);

    this.setupBankDataForm(this.fb);

  }

  // region Bank data operations
  setupBankDataForm(fb: FormBuilder) {

    this.completeForm = fb.group({});
    if (this.store.getProfessionalFormData('isPending') === true) {
      this.isProfessionalPending = true;
      // Get joinde data
      this.occupationControl = fb.control({
        value: this.store.getPendingProfessionalFormData('occupation'),
        disabled: true
      }, [this.requiredAfterTrim]);
      this.parentOccupationControl = fb.control({
          value: this.store.getPendingProfessionalFormData('parentOccupation'),
          disabled: true
        },
        [this.requiredAfterTrim]);
      this.rentTypeControl = fb.control(this.store.getPendingProfessionalFormData('rentType'),
        [this.requiredAfterTrim]);
      this.pensionTypeControl = fb.control(this.store.getPendingProfessionalFormData('pensionType'),
        [this.requiredAfterTrim]);
      this.activitySectorControl = fb.control(this.store.getPendingProfessionalFormData('activitySector'),
        [this.requiredAfterTrim]);
      this.activityControl = fb.control(this.store.getPendingProfessionalFormData('activity'),
        [this.requiredAfterTrim]);
      this.timeInEmployementControl = fb.control(this.store.getPendingProfessionalFormData('timeInEmployement'),
        [this.requiredAfterTrim]);
      this.addressControl = fb.control(this.store.getPendingProfessionalFormData('address'), []);
      this.cityControl = fb.control(this.store.getPendingProfessionalFormData('city'), []);
      this.provinceControl = fb.control(this.store.getPendingProfessionalFormData('province'), []);
      this.phoneControl = fb.control(this.store.getPendingProfessionalFormData('phone'),
        [this.phoneValidator]);
      this.mensualIncomesControl = fb.control(this.store.getPendingProfessionalFormData('mensualIncomes'),
        [this.requiredAfterTrim]);
      this.companyNameControl = fb.control(this.store.getPendingProfessionalFormData('companyName'),
        [this.requiredAfterTrim]);
      this.foreignTypeControl = fb.control(this.store.getPendingProfessionalFormData('foreignType'),
        [this.requiredAfterTrim]);

      this.completeForm.addControl('foreignType', this.foreignTypeControl);
      this.completeForm.addControl('occupation', this.occupationControl);
      this.completeForm.addControl('parentOccupation', this.parentOccupationControl);
      this.completeForm.addControl('companyName', this.companyNameControl);
      this.completeForm.addControl('rentType', this.rentTypeControl);
      this.completeForm.addControl('pensionType', this.pensionTypeControl);
      this.completeForm.addControl('activitySector', this.activitySectorControl);
      this.completeForm.addControl('activity', this.activityControl);
      this.completeForm.addControl('timeInEmployement', this.timeInEmployementControl);
      this.completeForm.addControl('address', this.addressControl);
      this.completeForm.addControl('city', this.cityControl);
      this.completeForm.addControl('province', this.provinceControl);
      this.completeForm.addControl('phone', this.phoneControl);

      this.professionalTypeSelected = this.parentOccupationControl.value;

    }
    if (this.store.getBankFormData('isPending') === true) {
      this.isBankDataPending = true;
      this.ibanValidator = new IBANValidatorGenerator();

      this.ibanForm = fb.control(this.store.getPendingIbanBankFormData(), [this.ibanValidator.validator()]);
      this.ibanFormValue = this.store.getPendingIbanBankFormData();

      this.completeForm.addControl('iban', this.ibanForm);

    }
  }

  setIBANField(event) {
    this.ibanForm.setValue(event);
  }

  // endregion
  checkProfessionalDataErrors(): string[] {
    let errors: string[] = [];
    let lastErrors = errors.map(x => Object.assign({}, x));
    this.errorsToPending = false;

    switch (this.professionalTypeSelected) {
      case Constants.OCCUPATION_FOREIGN:
        this.errorsToPending = this.checkAndAddError(this.foreignTypeControl, 'required',
          'Debe especificar el tipo de ocupación por cuenta ajena', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.companyNameControl, 'required',
          'Debe especificar el nombre de la empresa/organismo', errors) || this.errorsToPending;

      case Constants.OCCUPATION_OWN:
        this.errorsToPending = this.checkAndAddError(this.activitySectorControl, 'required',
          'Debe especificar el sector de actividad', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.activityControl, 'required',
          'Debe especificar la actividad', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.timeInEmployementControl, 'required',
          'Debe especificar la antigüedad', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.timeInEmployementControl, 'workingAge',
          'Antigüedad incorrecta', errors) || this.errorsToPending;
        this.errorsToPending = this.checkAndAddError(this.phoneControl, 'wrongNumber',
          'Número de teléfono incorrecto', errors) || this.errorsToPending;
        break;
    }
    if (this.occupationControl.value === Constants.OCCUPATION_HOUSEWIFE && this.errorsToPending === true && lastErrors.length === 0) {

    } else {
      this.errorsToPending = false;
    }
    console.dir(errors);
    this.professionalDataErrors = this.removeDuplicates(errors);
    return this.professionalDataErrors;
  }

  protected saveAndSend() {
    this.saveRequestForm();
  }

  checkFormErrors() {
    let errors = [];
    let isPending = false;


    if (this.isProfessionalPending) {
      this.store.setPendingProfessionalFormData({
        foreignType: this.foreignTypeControl.value,
        occupation: this.occupationControl.value,
        parentOccupation: this.parentOccupationControl.value,
        companyName: this.companyNameControl.value,
        rentType: this.rentTypeControl.value,
        pensionType: this.pensionTypeControl.value,
        activitySector: this.activitySectorControl.value,
        activity: this.activityControl.value,
        timeInEmployement: this.timeInEmployementControl.value,
        address: this.addressControl.value,
        city: this.cityControl.value,
        province: this.provinceControl.value,
        phone: this.phoneControl.value,
        mensualIncomes: this.mensualIncomesControl.value
      });
      const professionalErrors = this.checkProfessionalDataErrors();
      if (professionalErrors) {
        errors = errors.concat(professionalErrors);
      }
    }
    if (this.isBankDataPending) {
      this.ibanForm.markAsDirty();
      if (this.ibanForm.hasError('iban')) {
        console.error('IBAN incorrect');
        errors.push('Iban incorrecto');

      }
      this.store.setPendingIbanBankFormData(this.ibanForm.value);
      console.log('El iban introducido es ' + this.ibanForm.value);
    }
    if (errors.length > 0) {
      isPending = true;
      this.context.confirmDialog(
        errors.join('\n\n') +
        '\n\nSi continua la solicitud continuará en estado pendiente. ¿Desea continuar?', 'Errores detectados',
        ['Cancelar', 'Continuar'], (butIndex) => {
          if (butIndex === 2) {
            this.updatePendingForm(isPending);
          }
        });
    } else {
      this.updatePendingForm(isPending);
    }

  };

  updatePendingForm(isPending: boolean) {
    this._loadingSvc.setValue(true);

    this.server.updatePendingForm(isPending, (err, result) => {
      this._loadingSvc.setValue(false);
      if (err) {

      } else {
        this.headerClicked('prev');
      }
    });
  }

  headerClicked(event) {

    switch (event) {
      case 'next':
        this.checkFormErrors();
        break;
      case 'prev' :
        this.server.loadRequestsListByStatus('DONE_PENDING', (res) => {
          this._loadingSvc.setValue(false);
          this.router.navigate(['/lista', 'DONE_PENDING']);
        }, function (err) {
          this.router.navigate(['/']);
        });
        break;

      default:
        this.context.modalDialog('Form-complete.btnClicked evento desconocido: ' + event);
        return;

    }


  }
}
