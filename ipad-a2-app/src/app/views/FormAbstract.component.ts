import {Component, NgZone, OnInit} from '@angular/core';
import {ContextService} from "../services/context.service";
import {Router} from "@angular/router";
import {FormBuilder, FormControl} from '@angular/forms';
import {StoreService} from "../services/store.service";
import {ServerService} from "../services/server.service";
import {LoadingAnimateService} from "ng2-loading-animate";
import {environment} from '../../environments/environment';
import {RequestPrototypeDebug} from "../models/request";
import {RequiredAfterTrimValidator} from "../validators/RequiredAfterTrim.validator";
import {isArray} from "rxjs/util/isArray";

declare var $: any;
declare var window: any;
export abstract class FormAbstractComponent implements OnInit {
  protected routeToGo: any = "";
  protected environment: any = environment;
  protected requiredAfterTrim=new RequiredAfterTrimValidator().validator();

  constructor(protected store: StoreService, protected context: ContextService, protected router: Router, protected fb: FormBuilder, protected server: ServerService, protected _loadingSvc: LoadingAnimateService,protected ngZone: NgZone) {
    this._loadingSvc.setValue(true);

  }

  public ngOnInit() {
    this.context.setBodyClass("");
    if (this.store.isLogged() == false) {
      this.context.resetApp(true);
      //this.router.navigate(['/welcome']);
    }

    //todo Recheck until development
    /*if (this.context.debugMode) {
        if (this.environment.autoLogin == true) {
          if (this.environment.autoLogin && this.environment.autoLogin == true && this.store.isLogged() == false && this.environment['loginInfo'] != undefined) {
            this.server.doUserLogin(environment.loginInfo.login, environment.loginInfo.password, environment.loginInfo.shop, function (error, res) {
              this.store.currentRequest = Object.assign({}, RequestPrototypeDebug);
            });
          }
        }
      }
    }*/
    //this.store.printCurrentRequest();
    setTimeout(function () {
      window.scrollTo(0, 1);
    }, 0);
  }

  public ngAfterViewInit() {
    this._loadingSvc.setValue(false);
  }

  protected checkAndAddError(control: FormControl, errorToFind: string, message: string, errors: string[]) {
    control.markAsDirty();
    if (control.hasError(errorToFind)) {
      console.error("Adding "+message);
      errors.push(message);
      console.dir(errors);
      return true;
    }
    return false;
  }

  protected checkAndAddSpecialError(controls: FormControl[], errorResult) {
    if (errorResult) {
      controls.forEach(function (control) {
        var errors = control.errors || {};
        Object.keys(errorResult).forEach((field) => {
          errors[field] = errorResult[field];
        });
        control.setErrors(errors);
      });
    }
  }

  protected removeDuplicates(errors: string[]): string[] {
    var results = [];
    errors.forEach((error, index) => {
      if (errors.indexOf(error) == index) {
        results.push(error);
      }
    })
    return results;
  }

  protected checkAndAddSpecialErrorC(control: FormControl, errorResult) {
    return this.checkAndAddSpecialError([control], errorResult);
  }

  protected saveRequestForm() {
    this._loadingSvc.setValue(true);
    //this.store.printCurrentRequest();
    this.server.updateRequestForm((err) => {
      this._loadingSvc.setValue(false);
      if (!err) {
        var route=this.routeToGo;
        if(!isArray(this.routeToGo)){
          route=[this.routeToGo];
        }
        this.router.navigate(route);
      }
    });
  }

  protected tryToCancelDraftRequest() {
    this.context.confirmDialog("Si cancela la solicitud ya no podrá volver a recuperarla. ¿Esta seguro?", "Atención", ["Si", "No"], (button) => {
      if (button == 1) {
        this._loadingSvc.setValue(true);
        this.server.cancelDraftRequest((result) => {
          this._loadingSvc.setValue(false);
          this.context.resetApp(false);

//          this.router.navigate(["/welcome"]);
        }, function (err) {
          this._loadingSvc.setValue(false);

        })
      }
    })
  }

  protected abstract saveAndSend(callback);

  protected tryToSaveAsDraft(callback) {
    this.context.confirmDialog("¿Desea guardar la solicitud como borrador y volver al inicio?", "Atención", ["Guardar", "Cancelar"], (button) => {
      if (button == 1) {
        this._loadingSvc.setValue(true);
        this.routeToGo = "/welcome";
        this.saveAndSend(callback);
      }

    })
  }


}
