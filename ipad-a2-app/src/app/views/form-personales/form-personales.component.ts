import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ContextService} from "../../services/context.service";
import {Router} from "@angular/router";
import Constants from "../../constants";
import {FormBuilder, FormGroup, FormControl, Validators, Form} from '@angular/forms';
import {StoreService} from "../../services/store.service";
import {IDCardValidatorGenerator} from "../../validators/IDCard.validator";
import {AgeValidatorGenerator} from "../../validators/Age.validator";
import {RequiredIfControlValueGenerator} from "../../validators/RequiredIfControlValue.validator";
import {EmailValidator} from "../../validators/Email.validator"
import {PhoneValidatorGenerator} from "../../validators/Phone.validator";
import {FormAbstractComponent} from "../FormAbstract.component";
import {ServerService} from "../../services/server.service";
import {LoadingAnimateService} from "ng2-loading-animate";
declare var $: any;

@Component({
  selector: 'app-form-personales',
  templateUrl: 'form-personales.component.html',
  styleUrls: ['form-personales.component.scss']
})
export class FormPersonalesComponent extends FormAbstractComponent implements OnInit {

  //region Personal data values, attributes and validators
  days: string[] = [];
  months: string[] = Constants.months;
  countries: string[] = Constants.countries;
  years: string[] = Constants.years;

  personalDataForm: FormGroup;
  usernameControl: FormControl;
  surname1Control: FormControl;
  surname2Control: FormControl;
  idCodeControl: FormControl;
  idNumberControl: FormControl;
  genreControl: FormControl;
  birthDayControl: FormControl;
  birthMonthControl: FormControl;
  birthYearControl: FormControl;
  birthCountryControl: FormControl;
  birthCityControl: FormControl;
  nationalityControl: FormControl;

  ageValidatorGenerator: AgeValidatorGenerator;
  requiredIfControlValueGenerator: RequiredIfControlValueGenerator;
  idCardValidator: IDCardValidatorGenerator;

  personalDataErrors: string[] = [];
  //endregion

  constructor(store: StoreService, context: ContextService, router: Router, fb: FormBuilder, server: ServerService, protected _loadingSvc: LoadingAnimateService,protected ngZone: NgZone) {
    super(store, context, router, fb, server, _loadingSvc,ngZone);
    for (var i = 1; i < 31; i++) {
      this.days.push("" + i);
    }
    var theDate = new Date();

    for (var i = theDate.getFullYear() - 18; i > theDate.getFullYear() - 100; i--) {
      this.years.push("" + i);
    }
    this.setupPersonalForm(this.fb);
  }

  protected saveAndSend() {
    if(this.idNumberControl.value!="" && this.idNumberControl.valid) {
      this.savePersonalForm();
      this.saveRequestForm();
    }else{
      this._loadingSvc.setValue(false);
      this.context.modalDialog("Para poder almacenar la solicitud tiene que tener relleno al menos el Número de documento y ser válido","Error");
    }
  }

  headerClicked(event) {
    switch (event) {
      case "next":
        this.routeToGo = "/contacto";
        this.checkPersonalData();
        if (this.personalDataErrors.length == 0) {
          this.saveAndSend()
        }
        break;
      default:
        this.context.modalDialog("Form-personales.btnClicked evento desconocido: " + event);
        return;

    }


  }

  //region Personal Data operations
  setupPersonalForm(fb: FormBuilder) {
    this.idCardValidator = new IDCardValidatorGenerator();
    this.ageValidatorGenerator = new AgeValidatorGenerator();
    this.requiredIfControlValueGenerator = new RequiredIfControlValueGenerator();

    this.usernameControl = fb.control(this.store.getPersonalFormData('username'), [this.requiredAfterTrim]);
    this.surname1Control = fb.control(this.store.getPersonalFormData('surname1'), [this.requiredAfterTrim]);
    this.surname2Control = fb.control(this.store.getPersonalFormData('surname2'), [this.requiredIfControlValueGenerator.validator()]);
    this.idCodeControl = fb.control(this.store.getPersonalFormData('idCode'), [this.requiredAfterTrim]);
    this.idNumberControl = fb.control(this.store.getPersonalFormData('idNumber'), [this.requiredAfterTrim, this.idCardValidator.validator()]);
    this.genreControl = fb.control(this.store.getPersonalFormData('genre'), [this.requiredAfterTrim]);
    this.birthDayControl = fb.control(this.store.getPersonalFormData('birthDay'), [this.requiredAfterTrim, this.ageValidatorGenerator.validator()]);
    this.birthMonthControl = fb.control(this.store.getPersonalFormData('birthMonth'), [this.requiredAfterTrim, this.ageValidatorGenerator.validator()]);
    this.birthYearControl = fb.control(this.store.getPersonalFormData('birthYear'), [this.requiredAfterTrim, this.ageValidatorGenerator.validator()]);
    this.birthCountryControl = fb.control(this.store.getPersonalFormData('birthCountry'), [this.requiredAfterTrim]);
    this.birthCityControl = fb.control(this.store.getPersonalFormData('birthCity'), [this.requiredAfterTrim]);
    this.nationalityControl = fb.control(this.store.getPersonalFormData('nationality'), [this.requiredAfterTrim]);
    this.ageValidatorGenerator.setControls(this.birthDayControl, this.birthMonthControl, this.birthYearControl, null);
    this.requiredIfControlValueGenerator.setValueControl(this.idCodeControl, "NIF");

    this.idCardValidator.setControls(this.idCodeControl, this.idNumberControl);
    this.personalDataForm = fb.group({
      username: this.usernameControl,
      surname1: this.surname1Control,
      surname2: this.surname2Control,
      idCode: this.idCodeControl,
      idNumber: this.idNumberControl,
      genre: this.genreControl,
      birthDay: this.birthDayControl,
      birthMonth: this.birthMonthControl,
      birthYear: this.birthYearControl,
      birthCountry: this.birthCountryControl,
      birthCity: this.birthCityControl,
      nationality: this.nationalityControl,
    })
  }

  savePersonalForm() {
    this.store.setPersonalFormData({
      username: this.usernameControl.value,
      surname1: this.surname1Control.value,
      surname2: this.surname2Control.value,
      idCode: this.idCodeControl.value,
      idNumber: this.idNumberControl.value,
      genre: this.genreControl.value,
      birthDay: this.birthDayControl.value,
      birthMonth: this.birthMonthControl.value,
      birthYear: this.birthYearControl.value,
      birthCountry: this.birthCountryControl.value,
      birthCity: this.birthCityControl.value,
      nationality: this.nationalityControl.value,
    })
  }

  checkPersonalData() {
    var errors: string[] = [];

    this.checkAndAddError(this.usernameControl, 'required', 'Debe introducir el nombre', errors);
    this.checkAndAddError(this.surname1Control, 'required', 'Debe introducir el primer apellido', errors);
    this.checkAndAddError(this.surname2Control, 'requiredIf', 'Debe introducir el segundo apellido', errors);
    this.checkAndAddError(this.idNumberControl, 'required', 'Debe introducir el número de identificación', errors);
    this.checkAndAddError(this.genreControl, 'required', 'Debe seleccionar un género', errors);

    this.checkAndAddError(this.birthDayControl, 'required', 'Debe rellenar la fecha de nacimiento', errors);
    this.checkAndAddError(this.birthDayControl, 'wrongDate', 'Fecha de nacimiento incorrecta', errors);
    this.checkAndAddError(this.birthDayControl, 'tooYoung', 'Debe ser mayor de edad', errors);

    this.checkAndAddError(this.birthMonthControl, 'required', 'Debe rellenar la fecha de nacimiento', errors);
    this.checkAndAddError(this.birthMonthControl, 'wrongDate', 'Fecha de nacimiento incorrecta', errors);
    this.checkAndAddError(this.birthMonthControl, 'tooYoung', 'Debe ser mayor de edad', errors);

    this.checkAndAddError(this.birthYearControl, 'required', 'Debe rellenar la fecha de nacimiento', errors);
    this.checkAndAddError(this.birthYearControl, 'wrongDate', 'Fecha de nacimiento incorrecta', errors);
    this.checkAndAddError(this.birthYearControl, 'tooYoung', 'Debe ser mayor de edad', errors);

    this.checkAndAddError(this.birthCityControl, 'required', 'Debe indicar la ciudad de nacimiento', errors);
    this.checkAndAddError(this.nationalityControl, 'required', 'Debe indicar la nacionalidad', errors);
    this.checkAndAddError(this.idCodeControl, 'required', 'Debe indicar el tipo de documento', errors);
    this.checkAndAddError(this.idNumberControl, 'required', 'Debe indicar el número de documento', errors);
    this.checkAndAddError(this.idNumberControl, 'idCard', 'Número de documento incorrecto', errors);

    this.personalDataErrors = this.removeDuplicates(errors);
    return this.personalDataErrors;
  }

  //endregion

  privateOutsideFunc(command){
    this.context.modalDialog("Comando enviado a personales:"+command,"Desde fuera");
  }
}
