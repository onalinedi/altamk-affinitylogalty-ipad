import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPersonalesComponent } from './form-personales.component';

describe('FormPersonalesComponent', () => {
  let component: FormPersonalesComponent;
  let fixture: ComponentFixture<FormPersonalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPersonalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPersonalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
