import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ContextService} from '../../services/context.service';
import {Router} from '@angular/router';

import {FormBuilder, FormGroup, FormControl, Validators, Form} from '@angular/forms';
import {StoreService} from '../../services/store.service';

import {FormAbstractComponent} from '../FormAbstract.component';
import {ServerService} from '../../services/server.service';
import {LoadingAnimateService} from 'ng2-loading-animate';

declare var $: any;

@Component({
  selector: 'app-form-incidencias',
  templateUrl: 'form-incidencias.component.html',
  styleUrls: ['form-incidencias.component.scss']
})
export class FormIncidenciasComponent extends FormAbstractComponent implements OnInit {
  ticketForm: FormGroup;
  ticketErrors: string[] = [];
  txtTitle: FormControl;
  txtDescription: FormControl;

  constructor(store: StoreService, context: ContextService, router: Router, fb: FormBuilder,
              server: ServerService, protected _loadingSvc: LoadingAnimateService, protected ngZone: NgZone) {
    super(store, context, router, fb, server, _loadingSvc, ngZone);

  }

  ngOnInit() {
    this.context.setBodyClass('');
    this.txtTitle = this.fb.control('', [this.requiredAfterTrim]);
    this.txtDescription = this.fb.control('', [this.requiredAfterTrim]);
    this.ticketForm = this.fb.group({
      title: this.txtTitle,
      description: this.txtDescription
    });

  }

  protected saveAndSend() {
    this.router.navigate(this.routeToGo);
  }

  protected checkTicketData() {
    const errors: string[] = [];
    this.checkAndAddError(this.txtTitle, 'required', 'Debe introducir el título', errors);
    this.checkAndAddError(this.txtDescription, 'required',
      'Debe introducir la descripción', errors);
    return errors;
  }

  headerClicked(event) {

    switch (event) {
      case
      'prev':
        //this.routeToGo = ['/welcome'];
        //this.saveAndSend();
        this.context.resetApp(false);

        break;

      case 'next':
        if (this.checkTicketData().length === 0) {
          this._loadingSvc.setValue(true);

          this.server.sendTicket(this.txtTitle.value, this.txtDescription.value, (err, res) => {
            this._loadingSvc.setValue(false);
            if (err) {
              this.context.modalDialog(err, 'Ha ocurrido un error');
            } else {
              this.context.modalDialog(
                'Se ha enviado la incidencia y será estudiada por el equipo de desarrollo',
                'Info', () => {
                  this.headerClicked('prev');
                });
            }
          });
        }
        break;
      default:
        this.context.modalDialog('Form-incidencias.btnClicked evento desconocido: ' + event);
        return;

    }


  }


}
