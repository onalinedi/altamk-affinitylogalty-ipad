import {Routes} from '@angular/router';
import {WelcomeComponent} from './views/welcome/welcome.component'
import {PageNotFoundComponent} from './views/page-not-found/page-not-found.component'
import {FinishFormComponent} from './views/finish-form/finish-form.component'
import {FormPersonalesComponent} from './views/form-personales/form-personales.component'
import {FormContactoComponent} from './views/form-contacto/form-contacto.component';
import {FormOtrosPersonalesComponent} from './views/form-otros-personales/form-otros-personales.component';
import {FormProfesionalesComponent} from './views/form-profesionales/form-profesionales.component';
import {FormBancariosComponent} from './views/form-bancarios/form-bancarios.component';
import {FormDocumentacionComponent} from './views/form-documentacion/form-documentacion.component';
import {FormCompleteComponent} from './views/form-complete/form-complete.component';
import { RequestsListComponent } from './views/requests-list/requests-list.component';
import {FormIncidenciasComponent} from "./views/form-incidencias/form-incidencias.component";
export const appRoutes: Routes = [
  {path: 'welcome', component: WelcomeComponent},
  {path: 'personales', component: FormPersonalesComponent},
  {path: 'otros', component: FormOtrosPersonalesComponent},
  {path: 'profesionales', component: FormProfesionalesComponent},
  {path: 'bancarios', component: FormBancariosComponent},
  {path: 'contacto', component: FormContactoComponent},
  {path: 'documentacion', component: FormDocumentacionComponent},
  {path: 'documentacion2', component: FormDocumentacionComponent},
  {path: 'lista/:status', component: RequestsListComponent},
  {path: 'completar', component: FormCompleteComponent},
  {path: 'incidencias', component: FormIncidenciasComponent},
  {path: 'pdf', component: FinishFormComponent},
  {
    path: '',
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
  {path: '**', component: PageNotFoundComponent}
];
