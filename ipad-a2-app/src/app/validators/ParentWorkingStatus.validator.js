"use strict";
var ParentWorkingStatusValidatorGenerator = (function () {
    function ParentWorkingStatusValidatorGenerator() {
    }
    ParentWorkingStatusValidatorGenerator.prototype.setControls = function (statusControl, parentStatusControl) {
        this.workingStatusControl = statusControl;
        this.workingParentStatusControl = parentStatusControl;
    };
    ParentWorkingStatusValidatorGenerator.prototype.validatorFn = function () {
        if (!this.workingStatusControl || this.workingParentStatusControl) {
            return null;
        }
        if (this.workingStatusControl.value == "Ama de casa" && this.workingParentStatusControl.value == "") {
            return { required: { valid: false } };
        }
        else {
            return null;
        }
    };
    ParentWorkingStatusValidatorGenerator.prototype.validator = function () {
        var _this = this;
        return function (c) {
            if (c.value == "") {
                return null;
            }
            return _this.validatorFn();
        };
    };
    return ParentWorkingStatusValidatorGenerator;
}());
exports.ParentWorkingStatusValidatorGenerator = ParentWorkingStatusValidatorGenerator;
