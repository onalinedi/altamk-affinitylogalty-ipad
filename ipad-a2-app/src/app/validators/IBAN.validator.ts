import {FormControl} from '@angular/forms';
import {isValidIBAN} from 'ibantools';
import {IBANFormComponent} from "../components/ibanform-component/ibanform-component.component";


export class IBANValidatorGenerator {
  ibanComponent: IBANFormComponent;

  public validator() {
    return (c: FormControl) => {
      return {iban:!this.isValidIBAN(c.value)};
    };
  }

  public isValidIBAN(value) {
    if(!value || value.trim()==""){
      return true;
    }
    if(value.length!=24){
      return false;
    }
    if(!this.isValidCCC(value.substr(4))){
      return false;
    }
    return isValidIBAN(value);
  }

  public generateIBAN(numerocuenta, codigopais: string = "ES") {
    //ConversiÛn de letras por n˙meros
    //A=10 B=11 C=12 D=13 E=14
    //F=15 G=16 H=17 I=18 J=19
    //K=20 L=21 M=22 N=23 O=24
    //P=25 Q=26 R=27 S=28 T=29
    //U=30 V=31 W=32 X=33 Y=34
    //Z=35
    if (codigopais.length != 2)
      return "";
    else {
      var Aux;
      var CaracteresSiguientes;
      var TmpInt;
      var CaracteresSiguientes;

      numerocuenta = numerocuenta + (codigopais.charCodeAt(0) - 55).toString() + (codigopais.charCodeAt(1) - 55).toString() + "00";

      //Hay que calcular el mÛdulo 97 del valor contenido en n˙mero de cuenta
      //Como el n˙mero es muy grande vamos calculando mÛdulos 97 de 9 en 9 dÌgitos
      //Lo que se hace es calcular el mÛdulo 97 y al resto se le aÒaden 7 u 8 dÌgitos en funciÛn de que el resto sea de 1 Û 2 dÌgitos
      //Y asÌ sucesivamente hasta tratar todos los dÌgitos

      TmpInt = parseInt(numerocuenta.substring(0, 9), 10) % 97;

      if (TmpInt < 10)
        Aux = "0";
      else
        Aux = "";

      Aux = Aux + TmpInt.toString();
      numerocuenta = numerocuenta.substring(9);

      while (numerocuenta != "") {
        if (parseInt(Aux, 10) < 10)
          CaracteresSiguientes = 8;
        else
          CaracteresSiguientes = 7;

        if (numerocuenta.length < CaracteresSiguientes) {
          Aux = Aux + numerocuenta;
          numerocuenta = "";
        }
        else {
          Aux = Aux + numerocuenta.substring(0, CaracteresSiguientes);
          numerocuenta = numerocuenta.substring(CaracteresSiguientes);
        }

        TmpInt = parseInt(Aux, 10) % 97;

        if (TmpInt < 10)
          Aux = "0";
        else
          Aux = "";

        Aux = Aux + TmpInt.toString();
      }

      TmpInt = 98 - parseInt(Aux, 10);
      if(isNaN(TmpInt)){
        return codigopais+"  ";
      }
      if (TmpInt < 10)
        return codigopais + "0" + TmpInt.toString();
      else
        return codigopais + TmpInt.toString();

    }
  }

  public isValidCCC(value) {
    if (value.length != 20) {
      return false;
    }
    var banco = value.substring(0, 4);
    var sucursal = value.substring(4, 8);
    var dc = value.substring(8, 10);
    var cuenta = value.substring(10);
    var CCC = value;
    if ((banco.length > 0 && sucursal.length > 0 && dc.length > 0 && cuenta.length > 0 && banco + sucursal + dc + cuenta == 0) || (!/^[0-9]{20}$/.test(banco + sucursal + dc + cuenta))) {
      return false;
    }
    else {
      var valores = new Array(1, 2, 4, 8, 5, 10, 9, 7, 3, 6);
      var control = 0;
      var i;
      for (i = 0; i <= 9; i++)
        control += parseInt(cuenta.charAt(i)) * valores[i];
      control = 11 - (control % 11);
      if (control == 11)
        control = 0;
      else if (control == 10)
        control = 1;
      if (control != parseInt(dc.charAt(1))) {
        return false;
      }
      control = 0;
      var zbs = "00" + banco + sucursal;
      for (i = 0; i <= 9; i++)
        control += parseInt(zbs.charAt(i)) * valores[i];
      control = 11 - (control % 11);
      if (control == 11)
        control = 0;
      else if (control == 10)
        control = 1;
      if (control != parseInt(dc.charAt(0))) {
        return false;
      }
      return true;
    }
  }
}
