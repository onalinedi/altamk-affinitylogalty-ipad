import {FormControl} from '@angular/forms';
import Constants from "../constants";



export class ParentWorkingStatusValidatorGenerator {
  workingStatusControl: FormControl;
  workingParentStatusControl: FormControl;

  public setControls(statusControl: FormControl, parentStatusControl: FormControl) {
    this.workingStatusControl = statusControl;
    this.workingParentStatusControl = parentStatusControl;
  }

  public validatorFn() {
    if(!this.workingStatusControl || this.workingParentStatusControl){
      return null;
    }
    if (this.workingStatusControl.value == Constants.OCCUPATION_HOUSEWIFE && this.workingParentStatusControl.value == "") {
      return {required: {valid: false}}
    } else {
      return null;
    }
  }

  public validator() {
    return (c: FormControl) => {
      if (c.value == "") {
        return null
      }
      return this.validatorFn();
    };
  }
}
