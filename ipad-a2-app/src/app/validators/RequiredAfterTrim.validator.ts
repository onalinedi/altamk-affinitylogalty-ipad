import {FormControl} from '@angular/forms';

export class RequiredAfterTrimValidator {

  public validator() {
    return (c: FormControl) => {
      if (c.value==null || c.value==undefined || (""+c.value).trim() == "") {
        return {required:true}
      }
      return null;
    };
  }
}
