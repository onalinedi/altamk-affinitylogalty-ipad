import {FormControl} from '@angular/forms';

function isDNI(numero_introducido) {
  var value = numero_introducido.toUpperCase();
  // Basic format test
  if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {
    return false;
  }

  // Test NIF
  if (/^[0-9]{8}[A-Z]{1}$/.test(value)) {
    return ( "TRWAGMYFPDXBNJZSQVHLCKE".charAt(value.substring(8, 0) % 23) === value.charAt(8) );
  }
  // Test specials NIF (starts with K, L or M)
  if (/^[KLM]{1}/.test(value)) {
    return ( value[8] === String.fromCharCode(64) );
  }


  return false;
}

function isNIE(numero_introducido) {
  var value = numero_introducido.toUpperCase();
  // Basic format test
  if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {
    return false;
  }

  // Test NIE
  //T
  if (/^[T]{1}/.test(value)) {
    return ( value[8] === /^[T]{1}[A-Z0-9]{8}$/.test(value) );
  }

  //XYZ
  if (/^[XYZ]{1}/.test(value)) {
    return (
      value[8] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(
        value.replace('X', '0')
          .replace('Y', '1')
          .replace('Z', '2')
          .substring(0, 8) % 23
      )
    );
  }

  return false;
}


export class IDCardValidatorGenerator {
  typeControl: FormControl;
  valueControl: FormControl;

  public setControls(typeControl:FormControl, valueControl:FormControl) {
    this.typeControl = typeControl;
    this.valueControl = valueControl;
  }

  public validator(){
    return (c: FormControl) => {
      if (c.value == "") {
        return null
      }
      return this.validatorFn();
    };
  }

  protected validatorFn() {
  if (!this.typeControl || !this.valueControl) {
    return null;
  }
  const type: string = this.typeControl.value;
  const value: string = this.valueControl.value;

  var withError = false;
  switch (type) {
    case "DNI":
    case "NIF":
      withError = !isDNI(value);
      break;
    case "NIE":
      withError = !isNIE(value);
      break;
    default:
      withError = true;
      break;
  }
  if (withError) {
    return {idCard: true}
  } else {
    return null;
  }
}
}
