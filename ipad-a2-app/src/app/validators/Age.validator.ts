import {FormControl} from '@angular/forms';


export class AgeValidatorGenerator {
  dayControl: FormControl;
  monthControl: FormControl;
  yearControl: FormControl;
  borderElement: any;

  public setControls(day: FormControl, month: FormControl, year: FormControl, borderElement: any) {
    this.dayControl = day;
    this.monthControl = month;
    this.yearControl = year;
    this.borderElement = borderElement;
  }

  public validator() {
    return (c: FormControl) => {
      return this.validatorFn(c);
    };
  }

  protected setControlError(control: FormControl, errorField: string) {
    var errors = control.errors || {};
    errors[errorField] = true;
    control.setErrors(errors);
  }

  protected removeControlError(control: FormControl, errorField: string) {
    var errors = control.errors || {};
    delete(errors[errorField]);
    control.setErrors(errors);
  }

  protected removeAllControlErrors(control: FormControl) {
    control.setErrors(null);
  }

  protected validatorFn(c: FormControl) {

    if (!this.dayControl || !this.monthControl || !this.yearControl) {
      return null;
    }
    if (!this.dayControl.dirty || !this.monthControl.dirty || !this.yearControl.dirty) {
      return null;
    }
    var result = this.getAge(this.dayControl.value + "/" + this.monthControl.value + "/" + this.yearControl.value);
    //console.info("La edad es "+result);
    if (result == false) {
      this.setControlError(this.dayControl, "wrongDate");
      this.setControlError(this.monthControl, "wrongDate");
      this.setControlError(this.yearControl, "wrongDate");
    } else {
      this.removeControlError(this.dayControl, "wrongDate");
      this.removeControlError(this.monthControl, "wrongDate");
      this.removeControlError(this.yearControl, "wrongDate");

      if (result < 18) {
        this.setControlError(this.dayControl, "tooYoung");
        this.setControlError(this.monthControl, "tooYoung");
        this.setControlError(this.yearControl, "tooYoung");
      } else {
        this.removeAllControlErrors(this.dayControl);
        this.removeAllControlErrors(this.monthControl);
        this.removeAllControlErrors(this.yearControl);
      }
    }
    /**console.info("Errores day:"+JSON.stringify(this.dayControl.errors));
     console.info("Errores month:"+JSON.stringify(this.monthControl.errors));
     console.info("Errores year:"+JSON.stringify(this.yearControl.errors));**/
    return c.errors;
  }

  protected getAge(fecha): any {
    console.log("Age validator: la fecha es " + fecha)
    //calculo la fecha de hoy
    var hoy = new Date()
    //alert(hoy)

    //calculo la fecha que recibo
    //La descompongo en un array
    var array_fecha = fecha.split("/")
    //si el array no tiene tres partes, la fecha es incorrecta
    if (array_fecha.length != 3)
      return false


    //compruebo que los ano, mes, dia son correctos
    var ano
    ano = parseInt(array_fecha[2]);
    if (isNaN(ano))
      return false

    var mes
    mes = parseInt(array_fecha[1]);
    if (isNaN(mes))
      return false

    var dia
    dia = parseInt(array_fecha[0]);
    if (isNaN(dia))
      return false


    //si el año de la fecha que recibo solo tiene 2 cifras hay que cambiarlo a 4
    if (ano <= 99)
      ano += 1900

    // Compruebo que la fecha es correcta (por ejemplo, 29/2/1999)
    var template = new Date(ano, mes - 1, dia);//mes empieza de cero Enero = 0

    if (!template || !(template.getFullYear() == ano) || !(template.getMonth() == mes - 1) || !(template.getDate() == dia)) {
      return false;
    }

    //resto los años de las dos fechas
    var edad = hoy.getFullYear() - ano - 1; //-1 porque no se si ha cumplido años ya este año

    //si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
      return edad
    if (hoy.getMonth() + 1 - mes > 0)
      return edad + 1

    //entonces es que eran iguales. miro los dias
    //si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido
    if (hoy.getUTCDate() - dia >= 0)
      return edad + 1

    return edad

  }
}
