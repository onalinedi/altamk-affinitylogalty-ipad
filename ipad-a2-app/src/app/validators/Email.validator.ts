import {Directive, forwardRef} from '@angular/core';
import {NG_VALIDATORS, FormControl} from '@angular/forms';

function checkEmail(emailAddress) {
  var reValidEmail = new RegExp(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/);

  return reValidEmail.test(emailAddress);
}

export function EmailValidator(c: FormControl)  {
  return checkEmail(c.value) ? null :
    {email: true};

}

