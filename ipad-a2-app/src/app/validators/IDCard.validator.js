"use strict";
function isDNI(numero_introducido) {
    var value = numero_introducido.toUpperCase();
    // Basic format test
    if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {
        return false;
    }
    // Test NIF
    if (/^[0-9]{8}[A-Z]{1}$/.test(value)) {
        return ("TRWAGMYFPDXBNJZSQVHLCKE".charAt(value.substring(8, 0) % 23) === value.charAt(8));
    }
    // Test specials NIF (starts with K, L or M)
    if (/^[KLM]{1}/.test(value)) {
        return (value[8] === String.fromCharCode(64));
    }
    return false;
}
function isNIE(numero_introducido) {
    var value = numero_introducido.toUpperCase();
    // Basic format test
    if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {
        return false;
    }
    // Test NIE
    //T
    if (/^[T]{1}/.test(value)) {
        return (value[8] === /^[T]{1}[A-Z0-9]{8}$/.test(value));
    }
    //XYZ
    if (/^[XYZ]{1}/.test(value)) {
        return (value[8] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(value.replace('X', '0')
            .replace('Y', '1')
            .replace('Z', '2')
            .substring(0, 8) % 23));
    }
    return false;
}
var IDCardValidatorGenerator = (function () {
    function IDCardValidatorGenerator() {
    }
    IDCardValidatorGenerator.prototype.setControls = function (typeControl, valueControl) {
        this.typeControl = typeControl;
        this.valueControl = valueControl;
    };
    IDCardValidatorGenerator.prototype.validator = function () {
        var _this = this;
        return function (c) {
            if (c.value == "") {
                return null;
            }
            return _this.validatorFn();
        };
    };
    IDCardValidatorGenerator.prototype.validatorFn = function () {
        if (!this.typeControl || !this.valueControl) {
            return null;
        }
        var type = this.typeControl.value;
        var value = this.valueControl.value;
        var withError = false;
        switch (type) {
            case "DNI":
                withError = !isDNI(value);
                break;
            case "NIE":
                withError = !isNIE(value);
                break;
            default:
                withError = true;
                break;
        }
        if (withError) {
            return { idCard: true };
        }
        else {
            return null;
        }
    };
    return IDCardValidatorGenerator;
}());
exports.IDCardValidatorGenerator = IDCardValidatorGenerator;
