import {FormControl} from '@angular/forms';


export class RequiredIfControlValueGenerator {
  valueControl: FormControl;
  requiredValue:string;

  constructor(private errorName:string="requiredIf"){

  }
  public setValueControl(valueCtl: FormControl,value:string) {
    this.valueControl = valueCtl;
    this.requiredValue=value;
  }

  public validator() {
    return (c: FormControl) => {
      console.log("Checking...");
      return this.validatorFn(c);
    };
  }

  protected validatorFn(c:FormControl) {

    if (!this.valueControl) {
      return null;
    }
    if(this.valueControl.value==this.requiredValue && c.value==""){
      var res={}
      res[this.errorName]=true;
      return res;
    }else{
      return null;
    }
  }
}
