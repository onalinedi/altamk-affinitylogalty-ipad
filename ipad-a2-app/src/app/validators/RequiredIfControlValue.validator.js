"use strict";
var RequiredIfControlValueGenerator = (function () {
    function RequiredIfControlValueGenerator(errorName) {
        if (errorName === void 0) { errorName = "requiredIf"; }
        this.errorName = errorName;
    }
    RequiredIfControlValueGenerator.prototype.setValueControl = function (valueCtl, value) {
        this.valueControl = valueCtl;
        this.requiredValue = value;
    };
    RequiredIfControlValueGenerator.prototype.validator = function () {
        var _this = this;
        return function (c) {
            return _this.validatorFn(c);
        };
    };
    RequiredIfControlValueGenerator.prototype.validatorFn = function (c) {
        if (!this.valueControl) {
            return null;
        }
        if (this.valueControl.value == this.requiredValue && c.value == "") {
            var res = {};
            res[this.errorName] = true;
            return res;
        }
        else {
            return null;
        }
    };
    return RequiredIfControlValueGenerator;
}());
exports.RequiredIfControlValueGenerator = RequiredIfControlValueGenerator;
