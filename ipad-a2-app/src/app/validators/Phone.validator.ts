import {FormControl} from '@angular/forms';

export class PhoneValidatorGenerator {

  constructor(private type: string = "all") {

  }

  public validator() {
    return (c: FormControl) => {
      if (c.value == "") {
        return null
      }
      return this.validatorFn(c.value);
    };
  }

  protected validateBeginWith(allowed_numbers: number[], phone) {
    var value = parseInt(phone.substr(0, 1));
    if (allowed_numbers.indexOf(value) == -1) {
      return false;
    } else {
      return true;
    }
  }

  protected validatorFn(value) {
    // First, check if only numbers
    if (!/^\d+$/.exec(value) || value.length!=9) {
      return {wrongNumber: true}
    }
    var withError = false;

    switch (this.type) {
      case 'mobile':
        if (!this.validateBeginWith([6, 7], value)) {
          withError = true;
        }
        break;
      case 'home':
        if (!this.validateBeginWith([8,9], value)) {
          withError = true;
        }
        break;
      default:
        break;
    }
    if (withError) {
      return {wrongNumber: true}
    } else {
      return null;
    }
  }
}
