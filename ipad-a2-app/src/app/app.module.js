"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_1 = require('@angular/platform-browser');
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var app_component_1 = require('./app.component');
var welcome_component_1 = require('./welcome/welcome.component');
var login_form_component_1 = require('./login-form/login-form.component');
var sidebar_component_1 = require('./sidebar/sidebar.component');
var context_service_1 = require('./context.service');
var store_service_1 = require('./store.service');
var server_service_1 = require('./server.service');
var angular2_modal_1 = require('angular2-modal');
var bootstrap_1 = require('angular2-modal/plugins/bootstrap');
var forms_1 = require('@angular/forms');
var form_button_component_1 = require('./form-button/form-button.component');
var sidebar_info_component_1 = require('./sidebar-info/sidebar-info.component');
var router_1 = require('@angular/router');
var page_not_found_component_1 = require('./page-not-found/page-not-found.component');
var app_routes_1 = require('./app.routes');
var form1_component_1 = require('./form1/form1.component');
var process_header_component_1 = require('./process-header/process-header.component');
var form_progress_component_1 = require('./form-progress/form-progress.component');
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var form2_component_1 = require('./form2/form2.component');
var form_header_component_1 = require('./form-header/form-header.component');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                welcome_component_1.WelcomeComponent,
                login_form_component_1.LoginFormComponent,
                sidebar_component_1.SidebarComponent,
                form_button_component_1.FormButtonComponent,
                sidebar_info_component_1.SidebarInfoComponent,
                page_not_found_component_1.PageNotFoundComponent,
                form1_component_1.Form1Component,
                process_header_component_1.ProcessHeaderComponent,
                form_progress_component_1.FormProgressComponent,
                form2_component_1.Form2Component,
                form_header_component_1.FormHeaderComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                http_1.HttpModule,
                angular2_modal_1.ModalModule.forRoot(),
                bootstrap_1.BootstrapModalModule,
                router_1.RouterModule.forRoot(app_routes_1.appRoutes),
                ng_bootstrap_1.NgbProgressbarModule
            ],
            providers: [
                store_service_1.StoreService,
                context_service_1.ContextService,
                server_service_1.ServerService,
                ng_bootstrap_1.NgbProgressbarConfig
            ],
            bootstrap: [app_component_1.AppComponent],
            entryComponents: [login_form_component_1.LoginFormComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
