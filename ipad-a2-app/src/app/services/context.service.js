"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var bootstrap_1 = require('angular2-modal/plugins/bootstrap');
var ContextService = (function () {
    function ContextService(modal) {
        this.currentBodyClass = "welcomeBody";
        this.debugMode = false;
        this.modal = modal;
    }
    /**
     * Sets html body class
     * @param bodyClass
     */
    ContextService.prototype.setBodyClass = function (bodyClass) {
        if (bodyClass === void 0) { bodyClass = ""; }
        $('body').removeClass(this.currentBodyClass);
        this.currentBodyClass = bodyClass;
        $('body').addClass(this.currentBodyClass);
    };
    ContextService.prototype.modalDialog = function (message, title, callback) {
        if (title === void 0) { title = "Ha ocurrido un error"; }
        if (callback === void 0) { callback = null; }
        if (callback == null) {
            callback = function () { };
        }
        if (navigator && navigator['notification'] && navigator['notification']['alert']) {
            var fn = navigator['notification']['alert'];
            fn(message, callback, title);
        }
        else {
            this.modal.alert()
                .dialogClass('commonDialog')
                .okBtnClass('btn btn-sm btn-primary btn-centered')
                .size('sm')
                .showClose(false)
                .title(title)
                .body(message.split("\n").join("<br/>"))
                .open();
        }
    };
    ContextService.prototype.init = function () {
    };
    ContextService = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject(bootstrap_1.Modal))
    ], ContextService);
    return ContextService;
}());
exports.ContextService = ContextService;
