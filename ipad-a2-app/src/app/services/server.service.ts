import {Injectable, Inject} from '@angular/core';
import {Http, JsonpModule, Headers} from '@angular/http';
import {ContextService} from './context.service';
import {StoreService} from './store.service';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import ErrorResponses from '../../errorResponses';
import {FileUploader, FileItem} from 'ng2-file-upload';

declare let FileUploadOptions: any;
declare let FileTransfer: any;
declare var localStorage: any;

@Injectable()
export class ServerService {
  private userInfo: any;
  private context: ContextService;
  private store: StoreService;
  private lastShop: string;

  constructor(private http: Http,
              @Inject(ContextService) context: ContextService,
              @Inject(StoreService) store: StoreService) {
    this.context = context;
    this.store = store;
  }

  // region login/logout
  public doLogout() {
    const headers = new Headers();
    headers.append('x-access-token', this.store.getToken()['token']);
    this.http.get(
      environment.serverBaseURL + '/logout',
      {
        headers: headers
      }).subscribe(
      data => {
      },
      err => {
      },
      () => {
        console.info('Logout done');
      }
    );
  }

  protected getServerDocumentUploadURL(requestID: String, documentType: String) {
    return environment.serverBaseURL + '/api/requests/' + requestID + '/document/' + documentType;
  }

  public getServerBasedFilesURL(subpath: string): string {
    if (subpath == null) {
      return subpath;
    }
    return environment.serverBaseURL + '/files/' + subpath;
  }

  public uploadDocument(uploader: FileUploader, fileItem: FileItem, imageType: String, callback) {
    const headers: Headers = this.getRequestHeaders();
    const theHeaders = [];
    headers.keys().forEach((key) => {
      theHeaders.push({name: key, value: headers.get(key)});
    });
    theHeaders.push({name: 'filename', value: fileItem._file.name});

    uploader.setOptions({
      'url': this.getServerDocumentUploadURL(this.store.currentRequest.id, imageType),
      'method': 'POST',
      'headers': theHeaders
    });
    uploader.uploadItem(fileItem);
  }

  public uploadJpegFile(fileURL: string, imageType: String, callback) {
    const headers: Headers = this.getRequestHeaders();
    const theHeaders = {};
    headers.keys().forEach((key) => {
      console.log('- ' + key + ' :' + headers.get(key));
      theHeaders[key] = headers.get(key);
    });

    theHeaders['filename'] = 'file.jpg';

    console.log('** THE FILENAME IS ' + fileURL);
    console.log('** THE TARGET URL IS ' + this.getServerDocumentUploadURL(this.store.currentRequest.id, imageType));
    const options = new FileUploadOptions();
    options.fileKey = 'file';
    options.fileName = 'file.jpg';
    options.mimeType = 'image/jpeg';

    options.params = {};
    options.headers = theHeaders;

    const ft = new FileTransfer();
    ft.upload(fileURL, encodeURI(this.getServerDocumentUploadURL(this.store.currentRequest.id, imageType)),
      (successResult) => {
        callback(null, successResult);
      },
      (failResult) => {
        callback(failResult, null);
      }, options);
  }

  public doUserLogin(user, pass, shop, callback) {
    this.lastShop = shop;

    const headers = new Headers();
    headers.append('x-app-version', environment.version);
    if (callback === undefined) {
      callback = function (err, res) {
      };
    }
    this.http.post(
      environment.serverBaseURL + '/api/login/app',
      {
        username: user,
        password: pass,
        card: environment.cardCode,
        shop: shop
      },
      {
        headers: headers
      }).map(res => res.json())
      .subscribe(
        data => this.processUserLoginResult(data.result, callback),
        err => {
          try {
            err = JSON.parse(err._body);
          } catch (e) {
            if (err.status === 0) {
              this.context.modalDialog('No ha sido posible establecer una conexión con el servidor', 'Error');

            } else {
              this.context.modalDialog('Error desconocido en el servidor', 'Error');

            }
            this.store.setLoginInfo(null);
            return;
          }
          let error;
          switch (err.error.code) {
            case ErrorResponses.NO_CURRENT_ACTION_FOUND:
              error = 'No hay ninguna Acción activa en este momento';
              break;
            case ErrorResponses.NO_CURRENT_SHOP_FOUND:
              error = 'No existe ninguna tienda con ese código';
              break;
            case ErrorResponses.NO_USER_FOUND:
              error = 'Usuario no encontrado o contraseña incorrecta';
              break;
            case ErrorResponses.USER_UNAUTHORIZED:
              error = 'No tiene permisos para realizar solicitudes';
              break;
            case 200:
              break;
            default:
              error = 'Error deconocido en el servidor(' + err.error.code + ')';
              break;
          }
          this.context.modalDialog(error, 'Ha ocurrido un error');
          this.store.setLoginInfo(null);

        },
        () => {
          // console.log('api/v1/login/app ended')
        }
      );
  }

  protected processUserLoginResult(userLoginResult, callback) {
    const error = null;
    // todo:enable after development
    if (localStorage.getItem('altamk_show_login_welcome_' + environment.cardCode) == true) {

      this.context.modalDialog('Se ha conectado como\n' + userLoginResult.user.name + '', 'Bienvenido');
    }
    // Preprocess data
    userLoginResult.accessToken.endsIn = (+new Date()) - (1000 * 60 * 60) + (userLoginResult.accessToken.ttl);

    this.store.setLoginInfo(userLoginResult);
    callback(null, true);

  }

  // endregion

  public updateRequestForm(callback) {
    const headers = new Headers();
    headers.append('x-access-token', this.store.getToken()['token']);
    headers.append('x-app-version', environment.version);
    const theRequest = this.prepareCurrentRequestToSend();
    let path = environment.serverBaseURL + '/api/requests';
    if (this.store.currentRequest.id !== '') {
      path = path + '/' + this.store.currentRequest.id;
    }
    this.http.patch(
      path,
      theRequest,
      {
        headers: headers
      }).map(res => res.json())
      .subscribe(
        result => {
          this.extractDataFromServerRequest(result, ['form', 'documents']);
          callback(null);
        },
        err => {
          this.context.modalDialog('Ha ocurrido un error al grabar la solicitud (' + err.status + '):' + err.statusText, 'Error');
          callback(err);
        },
        () => {
          // console.log('api/v1/login/app ended')
        }
      );
  }

  public sendTicket(title, description, callback) {
    const headers = new Headers();
    headers.append('x-access-token', this.store.getToken()['token']);
    headers.append('x-app-version', environment.version);
    headers.append('x-card', this.store.card.id);
    headers.append('x-shop', this.store.shop.id);
    headers.append('x-action', this.store.action.id);
    headers.append('x-user', this.store.user.id);

    const body = {
      title: title,
      description: description,
      author: '[' + [
        this.store.card.code.toUpperCase(),
        this.store.user.name,
        this.store.shop.code,
        this.store.action.code
      ].join(',') +
      ']'
    };
    const path = environment.serverBaseURL + '/api/tickets/add/ipad';
    this.http.post(
      path,
      body,
      {
        headers: headers
      }).map(res => res.json())
      .subscribe(
        result => {
          callback(null, {});
        },
        err => {
          callback(err);
        },
        () => {
          // console.log('api/v1/login/app ended')
        }
      );
  }

  public updatePendingForm(isPending, callback) {
    const headers = new Headers();
    headers.append('x-access-token', this.store.getToken()['token']);
    headers.append('x-app-version', environment.version);
    headers.append('x-card', this.store.card.id);
    headers.append('x-shop', this.store.shop.id);
    headers.append('x-action', this.store.action.id);
    headers.append('x-user', this.store.user.id);

    let path = environment.serverBaseURL + '/api/requests';
    if (this.store.currentRequest.id !== '') {
      path = path + '/' + this.store.currentRequest.id + '/updatePending/' +
        (isPending ? 'DONE_PENDING' : 'DONE_COMPLETED');
    }
    this.http.put(
      path,
      JSON.parse(JSON.stringify(this.store.currentRequest.form.pending)),
      {
        headers: headers
      }).map(res => res.json())
      .subscribe(
        result => {
          this.context.modalDialog('Solicitud actualizada con estado ' +
            (isPending ? 'PENDIENTE' : 'COMPLETADA'), '', () => {
            callback(null, null);
          });
        },
        err => {
          this.context.modalDialog('Ha ocurrido un error al actualizar la solicitud (' + err.status + '):' +
            err.statusText, 'Error', () => {
            callback(err);
          });
        },
        () => {
          // console.log('api/v1/login/app ended')
        }
      );
  }

  protected prepareCurrentRequestToSend() {
    const req = JSON.parse(JSON.stringify(this.store.currentRequest));
    delete(req.id);
    req.cardId = this.store.card.id;
    req.actionId = this.store.action.id;
    req.shopId = this.store.shop.id;
    req.sender = this.store.user.id;
    return req;
  }

  protected extractDataFromServerRequest(serverRequestDocument, fields) {
    const req: any = {};
    if (!Array.isArray(fields)) {
      fields = [fields];
    }
    req.id = serverRequestDocument.id;
    req.id_code = serverRequestDocument.id_code;
    req.userId = serverRequestDocument.userId;
    fields.forEach((fieldName) => {
      req[fieldName] = serverRequestDocument[fieldName];
    });
    this.store.currentRequest = req;
  }

  public prepareRequestToLogalty(dataFn, errorFn) {
    const currentRequest = this.store.currentRequest;
    this.doPut(
      '/api/requests/' + currentRequest.id + '/changeStatus/LOGALTY_PENDING',
      {},
      dataFn,
      errorFn
    );

  }

  public beginLogaltyProcess(dataFn, errorFn) {
    const currentRequest = this.store.currentRequest;
    this.doPut(
      '/api/requests/' + currentRequest.id + '/changeStatus/LOGALTY_PROCESS_BEGIN',
      {},
      dataFn,
      errorFn
    );
  }

  public cancelDraftRequest(successFn, errorFn) {

    const currentRequest = this.store.currentRequest;
    if (currentRequest === undefined || currentRequest.id === undefined || currentRequest.id === '') {
      successFn();
    } else {
      this.doPut(
        '/api/requests/' + currentRequest.id + '/changeStatus/DRAFT_CANCELLED',
        {},
        successFn,
        errorFn
      );
    }
  }

  public cancelLogaltyRequest(successFn, errorFn) {
    const currentRequest = this.store.currentRequest;
    if (currentRequest === undefined || currentRequest.id === undefined || currentRequest.id === '') {
      successFn();
    } else {
      this.doPut(
        '/api/requests/' + currentRequest.id + '/logaltyCancel',
        {},
        (result) => {
          console.log('Recibida confirmación de cancel Logalty');
          successFn(result);
        },
        (error) => {
          console.log('Recibido error de cancel Logalty');
          errorFn(error);
        },
      );
    }
  }

  public commonRequestError(errResponse, suberrorFn) {
    console.dir(errResponse);

    if (errResponse.status !== undefined) {
      switch (errResponse.status) {
        case 404:
          this.context.modalDialog('A ocurrido un error al contactar con el servidor (404-No encontrado)\n' +
            'Por favor, inténtelo de nuevo pasados unos minutos', 'Error');
          break;
        case 403:
          this.context.modalDialog('No tiene permisos para realizar esta acción', 'Error');
          break;
        case 0:
          this.context.modalDialog('No ha sido posible establecer una conexión con el servidor', 'Error');
          break;
        case 500:
          let body = {error: {message: '' + new Date()}};
          try {
            body = JSON.parse(errResponse._body);
          } catch (e) {
            console.error(e);
          }
          this.context.modalDialog('A ocurrido un error interno en el servidor\n' + body.error.message, 'Error');
          break;
        default:
          suberrorFn(errResponse);
      }
    } else {
      suberrorFn(errResponse);
    }

  }

  protected getRequestHeaders() {
    const headers = new Headers();
    headers.append('x-card', this.store.card.id);
    headers.append('x-shop', this.store.shop.id);
    headers.append('x-action', this.store.action.id);
    headers.append('x-user', this.store.user.id);
    headers.append('x-app-version', environment.version);
    // todo: check for token caduced
    headers.append('x-access-token', this.store.getToken()['token']);
    return headers;
  }

  public getRequestFormPdfURL(dataFn, serverErrorFn) {
    return this.doGet('/api/requests/' + this.store.currentRequest.id + '/formpdf',
      dataFn, serverErrorFn);
  }

  public loadRequestsListByStatus(status, dataFn, serverErrorFn) {

    return this.doGet('/api/requests/status/' + status,
      (data) => {
        console.log('Returned requests list');
        this.store.setRequestsList(data.result);
        dataFn(data);
      }, serverErrorFn);
  }

  public changeRequestStatus(newStatus, okFn, errFn) {
    console.log('Changing ' + this.store.currentRequest.id + ' request status to ' + newStatus);
    this.doPut(
      '/api/requests/' + this.store.currentRequest.id + '/changeStatus/' + newStatus,
      {},
      okFn,
      errFn);


  }

  public getRequest(reqId, okFn, errFn) {
    console.log('Loading ' + reqId + ' request');
    this.doGet('/api/cards/' + this.store.card.id + '/requests/' + reqId,
      (response) => {

        okFn(response);
      }, errFn);
  }

  public doGet(subroute, dataFn, serverErrorFn) {
    this.http.get(
      environment.serverBaseURL + subroute,
      {
        headers: this.getRequestHeaders()
      }).map(res => res.json())
      .subscribe(
        result => {
          dataFn(result);
        }
        ,
        err => {
          this.commonRequestError(err, serverErrorFn);
        },
        () => {
          // console.log('api/v1/login/app ended')
        }
      );
  }

  protected doPut(subroute, body, dataFn, serverErrorFn) {
    this.http.put(
      environment.serverBaseURL + subroute,
      body,
      {
        headers: this.getRequestHeaders()
      }).map(res => res.json())
      .subscribe(
        result => {
          dataFn(result.data);

        },
        err => {
          this.commonRequestError(err, serverErrorFn);
        },
        () => {
          // console.log('api/v1/login/app ended')
        }
      );
  }

  protected doPatch(subroute, body, dataFn, serverErrorFn) {
    this.http.patch(
      environment.serverBaseURL + subroute,
      body,
      {
        headers: this.getRequestHeaders()
      }).map(res => res.json())
      .subscribe(
        result => {
          if (result.code && result.code === 200) {
            dataFn(null, result.data);
          } else {
            dataFn(result.code, result.error);
          }
        },
        err => {
          this.commonRequestError(err, serverErrorFn);
        },
        () => {
          // console.log('api/v1/login/app ended')
        }
      );
  }
}
