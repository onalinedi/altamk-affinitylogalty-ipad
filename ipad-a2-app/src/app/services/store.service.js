"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var rxjs_1 = require("rxjs");
var request_1 = require('./../models/request');
var context_service_1 = require("./context.service");
var StoreService = (function () {
    function StoreService(context) {
        this.user = {};
        this.token = {};
        this.shop = "";
        this.loggedObservable = new rxjs_1.BehaviorSubject("not logged");
        this.context = context;
        this.currentRequest = Object.assign({}, this.context.debugMode ? request_1.RequestPrototypeDebug : request_1.RequestPrototype);
    }
    StoreService.prototype.setLoginInfo = function (loginInfo) {
        if (loginInfo == null) {
            this.user = {};
            this.token = {};
            this.loggedObservable.next("not logged");
        }
        else {
            this.user = loginInfo.user;
            this.token = loginInfo.token;
            this.shop = loginInfo.shop;
            this.loggedObservable.next("logged");
        }
    };
    StoreService.prototype.getUsername = function () {
        if (this.user && this.user['name']) {
            return this.user['name'];
        }
        return "Desconocido";
    };
    StoreService.prototype.getUserLogin = function () {
        if (this.user && this.user['login']) {
            return this.user['login'];
        }
        return "Desconocido";
    };
    StoreService.prototype.getShop = function () {
        if (this.shop) {
            return this.shop;
        }
        return "Desconocida";
    };
    StoreService.prototype.getSessionEnding = function () {
        if (this.token && this.token['endsIn']) {
            return this.token['endsIn'];
        }
        return +new Date();
    };
    StoreService.prototype.getLoggedObservable = function () {
        return this.loggedObservable.asObservable();
    };
    StoreService.prototype.isLogged = function () {
        return this.token != null && this.user != null;
    };
    StoreService.prototype.init = function () {
        this.loggedObservable.next(this.isLogged() ? "logged" : "not logged");
    };
    StoreService.prototype.getPersonalFormData = function (field) {
        if (field === void 0) { field = null; }
        if (field == null) {
            return this.currentRequest.form.personalForm;
        }
        else {
            if (this.currentRequest.form.personalForm[field] == undefined) {
                throw new Error("Field '" + field + "' not fount in  personalFormData model");
            }
            return this.currentRequest.form.personalForm[field];
        }
    };
    StoreService.prototype.getContactFormData = function (field) {
        if (field === void 0) { field = null; }
        if (field == null) {
            return this.currentRequest.form.contactForm;
        }
        else {
            if (this.currentRequest.form.contactForm[field] == undefined) {
                throw new Error("Field '" + field + "' not fount in  contactFormData model");
            }
            return this.currentRequest.form.contactForm[field];
        }
    };
    StoreService = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject(context_service_1.ContextService))
    ], StoreService);
    return StoreService;
}());
exports.StoreService = StoreService;
