"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var context_service_1 = require('./context.service.js');
var store_service_1 = require('./store.service.js');
var environment_1 = require('../../environments/environment');
require('rxjs/Rx');
var errorResponses_1 = require('../../errorResponses');
var ServerService = (function () {
    function ServerService(http, context, store) {
        this.http = http;
        this.context = context;
        this.store = store;
    }
    ServerService.prototype.doUserLogin = function (user, pass, shop, callback) {
        var _this = this;
        this.lastShop = shop;
        var headers = new http_1.Headers();
        headers.append("x-card", environment_1.environment.cardCode);
        headers.append("x-shop", shop);
        if (callback == undefined) {
            callback = function (err, res) {
            };
        }
        this.http.post(environment_1.environment.serverBaseURL + "/api/v1/login/app", {
            login: user,
            password: pass
        }, {
            headers: headers
        }).map(function (res) { return res.json(); })
            .subscribe(function (data) { return _this.processUserLoginResult(data, callback); }, function (err) {
            //TODO: Generate correct error

            console.error(err);
        }, function () {
            console.log("api/v1/login/app ended");
        });
    };
    ServerService.prototype.processUserLoginResult = function (userLoginResult, callback) {
        var error = null;
        switch (userLoginResult.code) {
            case errorResponses_1.default.NO_CURRENT_ACTION_FOUND:
                error = "No hay ninguna Acción activa en este momento";
                break;
            case errorResponses_1.default.NO_CURRENT_SHOP_FOUND:
                error = "No existe ninguna tienda con ese código";
                break;
            case errorResponses_1.default.NO_USER_FOUND:
                error = "Usuario no encontrado o contraseña incorrecta";
                break;
            case errorResponses_1.default.USER_UNAUTHORIZED:
                error = "No tiene permisos para realizar solicitudes";
                break;
            case 200:
                break;
            default:
                error = "Error deconocido en el servidor(" + userLoginResult.code + ")";
                break;
        }
        if (error) {
            this.context.modalDialog(error, "Ha ocurrido un error");
            this.store.setLoginInfo(null);
            callback(error, null);
        }
        else {
            this.context.modalDialog("Se ha conectado como\n" + userLoginResult.data.user.name + "", "Bienvenido");
            // Preprocess data
            userLoginResult.data.shop = this.lastShop;
            userLoginResult.data.token.endsIn = (+new Date()) - (1000 * 60 * 60) + userLoginResult.data.token.expiresIn;
            this.store.setLoginInfo(userLoginResult.data);
            callback(null, true);
        }
    };
    ServerService = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject(context_service_1.ContextService)),
        __param(2, core_1.Inject(store_service_1.StoreService))
    ], ServerService);
    return ServerService;
}());
exports.ServerService = ServerService;
