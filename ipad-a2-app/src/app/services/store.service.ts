import {Injectable, EventEmitter, Inject} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {RequestPrototype, RequestPrototypeDebug, checkFormDataValid} from '../models/request';
import {ContextService} from "./context.service";
import Constants from "../constants";
import {environment} from "../../environments/environment";

declare var localStorage: any;

@Injectable()
export class StoreService {


  public user: any = {}
  public token = {token: ""}
  public shop: any = {};
  public card: any = {};
  public action: any = {};
  public logged = false;
  public currentRequest;
  private context: ContextService;
  private _requestsList: any[] = [];

  loggedObservable: BehaviorSubject<string> = new BehaviorSubject<string>("not logged");

  constructor(@Inject(ContextService) context: ContextService) {
    this.context = context;
    this.currentRequest = Object.assign({}, this.context.debugMode ? RequestPrototypeDebug : RequestPrototype);
  }

  public setLoginInfo(loginInfo) {
    if (loginInfo == null) {
      this.user = {}
      this.token = {token: ""}
      this.action = "";
      this.shop = "";
      this.logged = false;
      this.loggedObservable.next("not logged");
    } else {
      localStorage.setItem('altamk_login_' + environment.cardCode, loginInfo.user.username);
      localStorage.setItem('altamk_shopCode_' + environment.cardCode, loginInfo.shop.code);
      this.user = loginInfo.user;
      this.token = loginInfo.accessToken;
      this.action = loginInfo.action;
      this.shop = loginInfo.shop;
      this.card = loginInfo.card;
      this.logged = true;
      this.loggedObservable.next("logged");
    }
  }

  public getUsername(): string {
    if (this.user && this.user['name']) {
      return this.user['name'];
    }
    return "Desconocido";

  }

  public getAction() {
    return this.action;
  }

  public getUserLogin(): string {
    if (this.user && this.user['login']) {
      return this.user['login'];
    }
    return "Desconocido";

  }

  public getRequestsList(): any[] {
    return this._requestsList;
  }

  public setRequestsList(value: any[]) {
    this._requestsList = value;
  }

  public getToken(): any {
    if (!this.token) {
      return "";
    } else {
      return this.token;
    }
  }

  public getShop(): string {
    if (this.shop) {
      return this.shop;
    }
    return "Desconocida";

  }

  public getSessionEnding(): number {
    if (this.token && this.token['endsIn']) {
      return this.token['endsIn'];
    }
    return +new Date();
  }

  public getLoggedObservable(): Observable<String> {
    return this.loggedObservable.asObservable();
  }

  public isLogged() {
    return this.logged;
  }

  public init() {
    this.loggedObservable.next(this.isLogged() ? "logged" : "not logged");
  }

  public getPersonalFormData(field: string = null) {
    if (field == null) {
      return this.currentRequest.form.personal;
    }
    else {
      if (this.currentRequest.form.personal[field] == undefined) {
        throw new Error("Field '" + field + "' not fount in  personalFormData model");
      }
      return this.currentRequest.form.personal[field];
    }
  }

  public setPersonalFormData(value) {
    this.currentRequest.form.personal = value;
    checkFormDataValid(this.currentRequest);
  }

  public getContactFormData(field: string = null) {
    if (field == null) {
      return this.currentRequest.form.contact;
    }
    else {
      if (this.currentRequest.form.contact[field] == undefined) {
        throw new Error("Field '" + field + "' not fount in  contactFormData model");
      }
      return this.currentRequest.form.contact[field];
    }
  }

  public setCurrentRequest(requestData) {
    this.currentRequest = requestData;
    //console.dir(requestData);
  }

  public setContactFormData(value) {
    this.currentRequest.form.contact = value;
    checkFormDataValid(this.currentRequest);
  }

  public getOtherPersonalFormData(field: string = null) {
    if (field == null) {
      return this.currentRequest.form.otherPersonal;
    }
    else {
      if (this.currentRequest.form.otherPersonal[field] == undefined) {
        throw new Error("Field '" + field + "' not fount in  otherPersonalFormData model");
      }
      return this.currentRequest.form.otherPersonal[field];
    }
  }

  public setOtherPersonalFormData(value) {
    this.currentRequest.form.otherPersonal = value;
    checkFormDataValid(this.currentRequest);
  }

  public getProfessionalFormData(field: string = null) {
    if (field == null) {
      return this.currentRequest.form.professional;
    }
    else {
      if (this.currentRequest.form.professional[field] == undefined) {
        throw new Error("Field '" + field + "' not fount in  professionalDataForm model");
      }
      return this.currentRequest.form.professional[field];
    }
  }

  public getPendingProfessionalFormData(field: string = null) {
    if (this.currentRequest.form.pending == undefined) {
      this.currentRequest.form.pending = {}
    }
    if (this.currentRequest.form.pending.professional == undefined) {
      this.currentRequest.form.pending.professional = {}
    }
    if (field == null) {
      return this.currentRequest.form.pending.professional;
    }
    else {
      if (this.checkIsNullOrEmpty(this.currentRequest.form.pending.professional[field])) {
        return this.getProfessionalFormData(field);
      }
      return this.currentRequest.form.pending.professional[field];
    }
  }

  public setPendingProfessionalFormData(value) {
    this.currentRequest.form.pending.professional = value;
  }

  public setProfessionalFormData(value) {
    this.currentRequest.form.professional = value;
  }

  public getBankFormData(field: string = null) {
    if (field == null) {
      return this.currentRequest.form.bank;
    }
    else {
      if (this.currentRequest.form.bank[field] == undefined) {
        throw new Error("Field '" + field + "' not fount in  bankFormData model");
      }
      return this.currentRequest.form.bank[field];
    }
  }

  protected boolean

  private checkIsNullOrEmpty(value): boolean {
    return value == null || (value.trim().length == 0);
  }

  public getPendingIbanBankFormData() {
    if (this.currentRequest.form.pending == undefined) {
      this.currentRequest.form.pending = {}
    }
    if (this.currentRequest.form.pending.bankAccount == undefined) {
      this.currentRequest.form.pending.bankAccount = {}
    }
    if (this.currentRequest.form.pending.bankAccount.iban == undefined) {
      this.currentRequest.form.pending.bankAccount.iban = ""
    }
    if (this.checkIsNullOrEmpty(this.currentRequest.form.pending.bankAccount.iban)) {
      this.currentRequest.form.pending.bankAccount.iban = this.currentRequest.form.bank.iban
    }
    return this.currentRequest.form.pending.bankAccount.iban;
  }


  public setPendingIbanBankFormData(iban: string) {
    if (this.currentRequest.form.pending == undefined) {
      this.currentRequest.form.pending = {}
    }
    if (this.currentRequest.form.pending.bankAccount == undefined) {
      this.currentRequest.form.pending.bankAccount = {}
    }
    this.currentRequest.form.pending.bankAccount.iban = iban;
  }

  public documentFilePath(type: string) {
    if (this.currentRequest == undefined) return null;
    if (this.currentRequest.documents == undefined) return null;
    if (this.currentRequest.documents[type] == undefined || this.currentRequest.documents[type] == "") return null;
    return this.currentRequest.documents[type];
  }

  public setBankFormData(value) {
    this.currentRequest.form.bank = value;
    checkFormDataValid(this.currentRequest);
  }

  public isPending() {
    return this.getProfessionalFormData("isPending") || this.getBankFormData("isPending");
  }

  public printCurrentRequest() {
    //console.log("***************************************");
    //console.log(JSON.stringify(this.currentRequest, null, 4));
    //console.log("***************************************");
  }
}
