import {Injectable, Inject} from '@angular/core';
import {Modal} from 'angular2-modal/plugins/bootstrap';
import {environment} from "../../environments/environment";

declare var $: any;
declare var navigator: any;
declare var device: any;
declare var window: any;
declare var localStorage: any;
declare var document: any;

@Injectable()
export class ContextService {
  private currentBodyClass: String = "welcomeBody";
  private modal: Modal;
  public debugMode: boolean = false;

  constructor(@Inject(Modal) modal: Modal) {
    this.modal = modal;
  }

  /**
   * Sets html body class
   * @param bodyClass
   */
  public setBodyClass(bodyClass: String = "") {
    $('body').removeClass(this.currentBodyClass);
    this.currentBodyClass = bodyClass;
    $('body').addClass(this.currentBodyClass);
    $('body').removeClass('modal-open');
  }


  public resetApp(withMesssage: boolean=false) {
    console.warn("****************");
    console.warn("Resetting app...");
    console.warn("****************");
    localStorage.setItem('altamk_show_login_welcome_' + environment.cardCode, withMesssage);
    document.location.href = 'index.html';
  }

  public modalDialog(message: string, title: string = "Ha ocurrido un error", callback: any = null) {
    if (callback == null) {
      callback = function () {
      }
    }

    if (navigator && navigator['notification'] && navigator['notification']['alert']) {
      var fn = navigator['notification']['alert'];
      fn(message, callback, title);
    } else {
      this.modal.alert()
        .dialogClass('commonDialog')
        .okBtnClass('btn btn-sm btn-primary btn-centered')
        .size('sm')
        .showClose(false)
        .title(title)
        .body(message.split("\n").join("<br/>"))
        .open().then((resultPromise) => {

        resultPromise.result.then((result) => {

            callback(result);
          },
          () => {

          });
      });
    }
  }


  public confirmDialog(message: string, title: string = "Confirmación", buttons: string[], callback: any = null) {
    if (callback == null) {
      callback = function () {
      }
    }

    if (navigator && navigator['notification'] && navigator['notification']['confirm']) {
      var fn = navigator['notification']['confirm'];
      fn(message, callback, title, buttons);
    } else {
      var response = this.modal.confirm()
        .dialogClass('commonDialog')
        .okBtnClass('btn btn-sm btn-primary')
        .cancelBtnClass('btn btn-sm btn-primary')
        .size('sm')
        .showClose(false)
        .title(title)
        .okBtn(buttons[0])
        .cancelBtn(buttons[1])
        .body(message.split("\n").join("<br/>"))
        .open()
        .then(dialog => dialog.result)
        .then(result => callback(1))
        .catch(err => callback(2));

    }
  }

  public isCordovaApp() {
    return !(window['cordova'] == undefined);
  }

  public isIpadMini() {
    if (window['cordova'] && device) {
      switch (device.model) {
        case 'iPad2,5':
        case 'iPad2,6':
        case 'iPad2,7':
        case 'iPad4,4':
        case 'iPad4,5':
        case 'iPad4,6':
        case 'iPad4,7':
        case 'iPad4,8':
        case 'iPad4,9':
        case 'iPad5,1':
        case 'iPad5,2':
          return true;
        default:
          return false;
      }
    } else {
      return false;
    }
  }

  public init() {

  }
}
