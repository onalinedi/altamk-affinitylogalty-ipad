"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ProcessHeaderComponent = (function () {
    function ProcessHeaderComponent() {
        this.btnClicked = new core_1.EventEmitter();
    }
    ProcessHeaderComponent.prototype.ngOnInit = function () {
        if (!this.prevIcon) {
            this.prevIcon = "fa-chevron-left";
        }
        if (!this.nextIcon) {
            this.nextIcon = "fa-chevron-right";
        }
    };
    ProcessHeaderComponent.prototype.doPrev = function () {
        this.btnClicked.emit("prev");
    };
    ProcessHeaderComponent.prototype.doNext = function () {
        this.btnClicked.emit("next");
    };
    __decorate([
        core_1.Input()
    ], ProcessHeaderComponent.prototype, "prev", void 0);
    __decorate([
        core_1.Input()
    ], ProcessHeaderComponent.prototype, "title", void 0);
    __decorate([
        core_1.Input()
    ], ProcessHeaderComponent.prototype, "next", void 0);
    __decorate([
        core_1.Input()
    ], ProcessHeaderComponent.prototype, "prevIcon", void 0);
    __decorate([
        core_1.Input()
    ], ProcessHeaderComponent.prototype, "nextIcon", void 0);
    __decorate([
        core_1.Output()
    ], ProcessHeaderComponent.prototype, "btnClicked", void 0);
    ProcessHeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-process-header',
            templateUrl: 'process-header.component.html',
            styleUrls: ['process-header.component.scss']
        })
    ], ProcessHeaderComponent);
    return ProcessHeaderComponent;
}());
exports.ProcessHeaderComponent = ProcessHeaderComponent;
