import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-process-header',
  templateUrl: 'process-header.component.html',
  styleUrls: ['process-header.component.scss']
})
export class ProcessHeaderComponent implements OnInit {

  @Input()
  prev:string;
  @Input()
  title:string;
  @Input()
  next:string;
  @Input()
  prevIcon:string;
  @Input()
  nextIcon:string;

  @Output()
  btnClicked:EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if(!this.prevIcon){
      this.prevIcon="fa-chevron-left"
    }
    if(!this.nextIcon){
      this.nextIcon="fa-chevron-right"
    }
  }

  doPrev(){
    this.btnClicked.emit("prev");
  }

  doNext(){
    this.btnClicked.emit("next");
  }
}
