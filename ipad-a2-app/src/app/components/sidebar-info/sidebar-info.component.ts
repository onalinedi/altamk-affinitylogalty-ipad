import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-sidebar-info',
  templateUrl: 'sidebar-info.component.html',
  styleUrls: ['sidebar-info.component.scss']
})
export class SidebarInfoComponent implements OnInit {
  @Input()
  title: string;
  @Input()
  content: string;

  constructor() {
  }

  ngOnInit() {
  }

}
