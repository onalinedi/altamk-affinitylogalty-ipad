import {Component, OnInit, Input,Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() leftTitle: string
  @Input() rightTitle: string
  hideLeft:boolean;
  hideRight:boolean;

  @Output("leftClick") leftClickEmitter:EventEmitter<string>;
  @Output("rightClick") rightClickEmitter:EventEmitter<string>;

  constructor() {
    // Left side
    if (this.leftTitle == undefined || this.leftTitle == null) {
      this.leftTitle = ""
    }
    this.hideLeft=this.leftTitle=="";
    this.leftClickEmitter=new EventEmitter<string>();

    // Right side
    if (this.rightTitle == undefined || this.rightTitle == null) {
      this.rightTitle = ""
    }
    this.hideRight=this.rightTitle=="";
    this.rightClickEmitter=new EventEmitter<string>();
  }

  ngOnInit() {
  }

  leftClick(){
    this.leftClickEmitter.emit(this.leftTitle);
  }
  rightClick(){
    this.rightClickEmitter.emit(this.rightTitle);
  }
}
