import {Component} from '@angular/core';

import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {ServerService} from '../../services/server.service';
import {ContextService} from "../../services/context.service";
import {environment} from "../../../environments/environment";

declare var localStorage:any;
export class LoginFormContext extends BSModalContext {
  public login: string;
  public password: string;
  public shop: string;
}


/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
@Component({
  selector: 'modal-content',
  templateUrl: 'login-form.component.html',
  styleUrls: ['login-form.component.scss']
})
export class LoginFormComponent implements CloseGuard, ModalComponent<LoginFormContext> {
  context: LoginFormContext;

  public wrongAnswer: boolean;

  constructor(public dialog: DialogRef<LoginFormContext>, private server: ServerService, private appContext: ContextService) {
    dialog.context.login=localStorage.getItem('altamk_login_'+environment.cardCode);

    dialog.context.password=localStorage.getItem('altamk_password_'+environment.cardCode);


    dialog.context.shop=localStorage.getItem('altamk_shopCode_'+environment.cardCode);
    this.context = dialog.context;
    this.context.dialogClass = "modal-dialog login-dialog";
    this.wrongAnswer = true;
    dialog.setCloseGuard(this);
  }


  beforeDismiss(): boolean {
    return true;
  }

  beforeClose(): boolean {
    return true;
  }

  login() {

    if (!this.context.login || this.context.login.trim() == "" || !this.context.password || this.context.password.trim() == "" || !this.context.shop || this.context.shop.trim() == "") {
      this.appContext.modalDialog("Debe rellenar todos los campos", "Error");
    }else{
      this.context.login=this.context.login.toLowerCase();
      this.context.shop=this.context.shop.toUpperCase();
      this.server.doUserLogin(this.context.login,this.context.password,this.context.shop,(error,result)=>{
        if(error){

        }else{
          this.dialog.destroy();
        }
      });
    }
  }

  hide() {
    this.dialog.destroy();
  }
}
