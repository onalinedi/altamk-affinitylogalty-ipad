"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var bootstrap_1 = require('angular2-modal/plugins/bootstrap');
var LoginFormContext = (function (_super) {
    __extends(LoginFormContext, _super);
    function LoginFormContext() {
        _super.apply(this, arguments);
    }
    return LoginFormContext;
}(bootstrap_1.BSModalContext));
exports.LoginFormContext = LoginFormContext;
/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
var LoginFormComponent = (function () {
    function LoginFormComponent(dialog, server, appContext) {
        this.dialog = dialog;
        this.server = server;
        this.appContext = appContext;
        this.context = dialog.context;
        this.context.dialogClass = "modal-dialog login-dialog";
        this.wrongAnswer = true;
        dialog.setCloseGuard(this);
    }
    LoginFormComponent.prototype.beforeDismiss = function () {
        return true;
    };
    LoginFormComponent.prototype.beforeClose = function () {
        return true;
    };
    LoginFormComponent.prototype.login = function () {
        var _this = this;
        if (!this.context.login || this.context.login.trim() == "" || !this.context.password || this.context.password.trim() == "" || !this.context.shop || this.context.shop.trim() == "") {
            this.appContext.modalDialog("Debe rellenar todos los campos", "Error");
        }
        else {
            this.context.login = this.context.login.toLowerCase();
            this.context.shop = this.context.shop.toUpperCase();
            this.server.doUserLogin(this.context.login, this.context.password, this.context.shop, function (error, result) {
                if (error) {
                }
                else {
                    _this.dialog.destroy();
                }
            });
        }
    };
    LoginFormComponent.prototype.hide = function () {
        this.dialog.destroy();
    };
    LoginFormComponent = __decorate([
        core_1.Component({
            selector: 'modal-content',
            templateUrl: 'login-form.component.html',
            styleUrls: ['login-form.component.scss']
        })
    ], LoginFormComponent);
    return LoginFormComponent;
}());
exports.LoginFormComponent = LoginFormComponent;
