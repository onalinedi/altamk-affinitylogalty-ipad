import {Component, OnInit, trigger, state, style, transition, animate, NgZone} from '@angular/core';
import {ContextService} from '../../services/context.service';
import {StoreService} from "../../services/store.service";
import {ServerService} from "../../services/server.service";
import {environment} from "../../../environments/environment";
import {LoadingAnimateService} from "ng2-loading-animate";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['sidebar.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({width:'250px',
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({width:'0px',
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('300ms ease-in-out')),
      transition('out => in', animate('300ms 300ms ease-in-out'))
    ]),
  ]
})
export class SidebarComponent implements OnInit {
  public localState = {value: ''};
  public menuState: string = 'out';
  protected loggedObserver: any;
  protected loggedData;
  protected logged: boolean = false;
  protected environment: any = environment;

  constructor(private context: ContextService, private store: StoreService, private server: ServerService, protected _loadingSvc: LoadingAnimateService,private router:Router,private zone: NgZone) {
    console.log("App version " + this.environment.version);
  }

  ngOnInit() {
    //console.log('hello `Sidebar` component');
    this.context.setBodyClass('welcomeBody');
    this.cleanLoggedData();

    this.loggedObserver = this.store.getLoggedObservable().subscribe(msg => {
      this.processLoggedInfo();
      //console.log("SidebarComponent.loggedObserver: User is "+msg);
    })
    //console.log("SidebarComponent.ngInit: Subscribed to loggedObserver;")
  }

  ngOnDestroy() {
    this.loggedObserver.unsubscribe();
    //console.log("SidebarComponent.ngOnDestroy: Unsubscribed to loggedObserver;")
  }

  protected cleanLoggedData() {
    this.loggedData = {user: "No disponible", shop: "No disponible", session: "No disponible"};
  }

  public toggleMenu(forced: any = false) {
    // 1-line if statement that toggles the value:
    this.zone.run( () => {
      if (forced !== false) {
        this.menuState = forced;
      } else {
        this.menuState = this.menuState === 'out' ? 'in' : 'out';
      }
    });
    console.log("******** Sidebarmenustate="+this.menuState);
  }

  protected processLoggedInfo() {
    this.loggedData.user = this.store.getUsername();
    this.loggedData.shop = this.store.getShop();
    this.loggedData.session = new Date(this.store.getSessionEnding()).toLocaleString();
    this.loggedData.action = this.store.getAction();

  }

  public doLogout() {
    this.context.confirmDialog("¿Esta seguro de querer finalizar la sesión en esta tienda?", "Atención", ["Si", "No"], (button) => {
      if (button == 1) {
        this.toggleMenu("out");
        this.server.doLogout();
        this.store.setLoginInfo(null);
      }
    })
  }

  public doDraftsView() {
    this.toggleMenu("out");
    this._loadingSvc.setValue(true);
    this.server.loadRequestsListByStatus("DRAFT", (res)=> {
      this.router.navigate(["/lista","DRAFT"]);
    }, function (err) {
      this._loadingSvc.setValue(false);
    })
  }

  public doPendingsView() {
    this.toggleMenu("out");
    this._loadingSvc.setValue(true);
    this.server.loadRequestsListByStatus("DONE_PENDING", (res)=> {
      this.router.navigate(["/lista","DONE_PENDING"]);
    }, function (err) {
      this._loadingSvc.setValue(false);
    })
  }

  public doTicket() {
    this.toggleMenu("out");
    this.router.navigate(["/incidencias"]);
  }

  public isVisible() {
    console.log("******** isVisible='in'=="+this.menuState);
    return this.menuState == "in";
  }
}
