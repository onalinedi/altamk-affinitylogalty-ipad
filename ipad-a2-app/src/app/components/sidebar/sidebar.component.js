"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var SidebarComponent = (function () {
    function SidebarComponent(context, store) {
        this.context = context;
        this.store = store;
        this.localState = { value: '' };
        this.menuState = 'out';
        this.logged = false;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        //console.log('hello `Sidebar` component');
        this.context.setBodyClass('welcomeBody');
        this.cleanLoggedData();
        this.loggedObserver = this.store.getLoggedObservable().subscribe(function (msg) {
            _this.processLoggedInfo();
            //console.log("SidebarComponent.loggedObserver: User is " + msg);
        });
        //console.log("SidebarComponent.ngInit: Subscribed to loggedObserver;");
    };
    SidebarComponent.prototype.ngOnDestroy = function () {
        this.loggedObserver.unsubscribe();
        //console.log("SidebarComponent.ngOnDestroy: Unsubscribed to loggedObserver;");
    };
    SidebarComponent.prototype.cleanLoggedData = function () {
        this.loggedData = { user: "No disponible", shop: "No disponible", session: "No disponible" };
    };
    SidebarComponent.prototype.toggleMenu = function (forced) {
        if (forced === void 0) { forced = false; }
        // 1-line if statement that toggles the value:
        if (forced !== false) {
            this.menuState = forced;
        }
        else {
            this.menuState = this.menuState === 'out' ? 'in' : 'out';
        }
    };
    SidebarComponent.prototype.processLoggedInfo = function () {
        this.loggedData.user = this.store.getUsername();
        this.loggedData.shop = this.store.getShop();
        this.loggedData.session = this.store.getSessionEnding();
    };
    SidebarComponent.prototype.doLogout = function () {
        this.toggleMenu("out");
        this.store.setLoginInfo(null);
    };
    SidebarComponent.prototype.isVisible = function () {
        return this.menuState == "in";
    };
    SidebarComponent = __decorate([
        core_1.Component({
            selector: 'app-sidebar',
            templateUrl: 'sidebar.component.html',
            styleUrls: ['sidebar.component.scss'],
            animations: [
                core_1.trigger('slideInOut', [
                    core_1.state('in', core_1.style({
                        transform: 'translate3d(0, 0, 0)'
                    })),
                    core_1.state('out', core_1.style({
                        transform: 'translate3d(100%, 0, 0)'
                    })),
                    core_1.transition('in => out', core_1.animate('300ms ease-in-out')),
                    core_1.transition('out => in', core_1.animate('300ms ease-in-out'))
                ]),
            ]
        })
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;
