import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-form-progress',
  templateUrl: 'form-progress.component.html',
  styleUrls: ['form-progress.component.scss']
})
export class FormProgressComponent implements OnInit {
  @Input()
  value: string;

  constructor() {
  }

  ngOnInit() {
  }

}
