import {Component, OnInit, Output, EventEmitter, ViewChild, Input, Renderer} from '@angular/core';
import {IBANValidatorGenerator} from "../../validators/IBAN.validator";

function fixValueTo(value, to) {
  while (value.length < to) {
    value = " " + value;
  }
  return value;

}
@Component({
  selector: 'app-iban-form-component',
  templateUrl: 'ibanform-component.component.html',
  styleUrls: ['ibanform-component.component.scss']
})
export class IBANFormComponent implements OnInit {
  constructor(public renderer: Renderer) {
  }

  @Input()
  IBANValue: string;

  @Output()
  ibanChange: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('ibanCountry') countryField;
  @ViewChild('ibanCountryCode') countryCodeField;
  @ViewChild('ibanEntity') entityField;
  @ViewChild('ibanSucursal') sucursalField;
  @ViewChild('ibanControlDigit') controlDigitField;
  @ViewChild('ibanAccount') accountField;

  country: String = "";
  countryCode: string = "";
  entity: string = "";
  sucursal: string = "";
  controlDigit: string = "";
  account: string = "";


  statusClasses: string;
  fieldStatusClasses: string;
  validator: IBANValidatorGenerator = new IBANValidatorGenerator();

  ngOnInit() {
    this.processIBANValue();
  }

  protected processIBANValue() {
    //console.log("The obtained IBAN is "+this.IBANValue);
    this.country = this.IBANSubstring(0, 2).trim();
    this.countryCode = this.IBANSubstring(2, 2).trim();
    this.entity = this.IBANSubstring(4, 4).trim();
    this.sucursal = this.IBANSubstring(8, 4).trim();
    this.controlDigit = this.IBANSubstring(12, 2).trim();
    this.account = this.IBANSubstring(14, 10).trim();
  }

  protected IBANSubstring(begin, count) {
    if (!this.IBANValue || !this.IBANValue.length > begin) {
      return "";
    }
    if (!this.IBANValue.length > count) {
      count = this.IBANValue.length - begin;
    }
    return this.IBANValue.substr(begin, count);
  }

  onIbanChange() {
    this.IBANValue = fixValueTo(this.countryField.nativeElement.value, 2) +
      fixValueTo(this.countryCodeField.nativeElement.value, 2) +
      fixValueTo(this.entityField.nativeElement.value, 4) +
      fixValueTo(this.sucursalField.nativeElement.value, 4) +
      fixValueTo(this.controlDigitField.nativeElement.value, 2) +
      fixValueTo(this.accountField.nativeElement.value, 10);
    this.ibanChange.emit(this.IBANValue);
    //console.log("The IBAN code is "+this.IBANValue);
    if (!this.validator.isValidIBAN(this.IBANValue)) {
      this.fieldStatusClasses = "formBorderError";
    } else {
      this.fieldStatusClasses = "";
    }
  }

  public check() {
    this.onIbanChange();
  }

  public generateIBANCodes() {
    while (this.IBANValue.length < 22) {
      this.IBANValue = " " + this.IBANValue;
    }
    while (this.IBANValue.length > 20) {
      this.IBANValue = this.IBANValue.substr(1);
    }
    var theValue = this.validator.generateIBAN(this.IBANValue);
    this.country = this.countryField.nativeElement.value = theValue.substr(0, 2);
    this.countryCode = this.countryCodeField.nativeElement.value = theValue.substr(2, 4);

    this.onIbanChange();
  }

  processKeyUp(event, max, nextElement) {

    if ([37, 39, 8, 38, 40, 46,9,16].indexOf(event.keyCode) >= 0) {
      return;
    }
    var target = event.target || event.srcElement || event.currentTarget;
    var length = target.value.trim().length;
    console.log("key:" + event.keyCode + " - length " + length + " - max " + max + " - next:" + nextElement);
    var nextFormControl = null;
    if (length == max || event.keyCode == 13) {
      switch (nextElement) {
        case 'countryCode':
          nextFormControl = this.countryCodeField;
          break;
        case 'entity':
          nextFormControl = this.entityField;
          break;
        case 'sucursal':
          nextFormControl = this.sucursalField;
          break;
        case 'controlDigit':
          nextFormControl = this.controlDigitField;
          break;
        case 'account':
          nextFormControl = this.accountField;
          break;
        default:
          break;
      }

      if (nextFormControl != null &&
        (event.keyCode == 13 ||
        (nextFormControl.nativeElement.value != undefined &&
        nextFormControl.nativeElement.value.trim() == ""))) {
        nextFormControl.nativeElement.value = "";
        this.renderer.invokeElementMethod(
          nextFormControl.nativeElement, 'focus', []);
      }
    }
  }
}
