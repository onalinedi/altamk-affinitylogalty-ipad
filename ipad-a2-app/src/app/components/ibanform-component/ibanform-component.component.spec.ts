import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IBANFormComponent } from './ibanform-component.component';

describe('IBANFormComponent', () => {
  let component: IBANFormComponent;
  let fixture: ComponentFixture<IBANFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IBANFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IBANFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
