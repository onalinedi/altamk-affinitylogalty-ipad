import {Component} from '@angular/core';

import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {ServerService} from '../../services/server.service';
import {ContextService} from "../../services/context.service";
import {BSModalContext} from "angular2-modal/plugins/bootstrap";


declare var localStorage: any;


export class QuestionFormContext extends BSModalContext {
  public question: string;
  public response: string;
  public callback: any;
  public title: string;
}

/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
@Component({
  selector: 'question-form',
  templateUrl: 'question-form.component.html',
  styleUrls: ['question-form.component.scss']
})
export class QuestionFormComponent implements CloseGuard, ModalComponent<QuestionFormContext> {
  context: QuestionFormContext;
  public wrongAnswer: boolean;

  constructor(public dialog: DialogRef<QuestionFormContext>, private server: ServerService, private appContext: ContextService) {
    /*dialog.context.question=localStorage.getItem('altamk_login_'+environment.cardCode);
     dialog.context.password=localStorage.getItem('altamk_password_'+environment.cardCode);
     dialog.context.shop=localStorage.getItem('altamk_shopCode_'+environment.cardCode);*/
    this.context = dialog.context;
    this.context.dialogClass = "modal-dialog login-dialog";
    this.wrongAnswer = true;
    dialog.setCloseGuard(this);
  }


  beforeDismiss(): boolean {
    return true;
  }

  beforeClose(): boolean {
    return true;
  }

  butOKPressed() {
    if (!this.context.response || this.context.response.trim() == "") {
      this.appContext.modalDialog("La respuesta no puede estar vacía", "Error");
    } else {
      this.context.response = this.context.response.toLowerCase();
      this.dialog.destroy();
      if (this.context.callback != undefined) {
        this.context.callback(null, this.context.response)
      }
    }
  }

  hide() {
    this.dialog.destroy();
    this.context.callback(null, null)
  }
}
