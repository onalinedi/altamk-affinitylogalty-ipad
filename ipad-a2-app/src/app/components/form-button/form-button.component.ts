import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-form-button',
  templateUrl: 'form-button.component.html',
  styleUrls: ['form-button.component.scss']
})
export class FormButtonComponent implements OnInit {
  @Input()
  icon: String;
  @Input()
  endIcon: String;
  @Input()
  text: String;
  @Input()
  btnClass: String;
  constructor() {
    if(!this.btnClass){
      this.btnClass="btn-form-altamk"
    }
    if(!this.endIcon){
      this.endIcon=""
    }
  }

  ngOnInit() {
  }


}
