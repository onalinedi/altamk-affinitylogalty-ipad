"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var FormButtonComponent = (function () {
    function FormButtonComponent() {
        if (!this.btnClass) {
            this.btnClass = "btn-form-altamk";
        }
        if (!this.endIcon) {
            this.endIcon = "";
        }
    }
    FormButtonComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Input()
    ], FormButtonComponent.prototype, "icon", void 0);
    __decorate([
        core_1.Input()
    ], FormButtonComponent.prototype, "endIcon", void 0);
    __decorate([
        core_1.Input()
    ], FormButtonComponent.prototype, "text", void 0);
    __decorate([
        core_1.Input()
    ], FormButtonComponent.prototype, "btnClass", void 0);
    FormButtonComponent = __decorate([
        core_1.Component({
            selector: 'app-form-button',
            templateUrl: 'form-button.component.html',
            styleUrls: ['form-button.component.scss']
        })
    ], FormButtonComponent);
    return FormButtonComponent;
}());
exports.FormButtonComponent = FormButtonComponent;
