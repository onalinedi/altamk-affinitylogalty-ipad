import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormErrorsPanelComponent } from './form-errors-panel.component';

describe('FormErrorsPanelComponent', () => {
  let component: FormErrorsPanelComponent;
  let fixture: ComponentFixture<FormErrorsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormErrorsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormErrorsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
