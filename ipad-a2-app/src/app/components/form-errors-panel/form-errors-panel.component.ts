import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-form-errors-panel',
  templateUrl: 'form-errors-panel.component.html',
  styleUrls: ['form-errors-panel.component.scss']
})
export class FormErrorsPanelComponent implements OnInit {
  @Input() errors;

  constructor() {
  }

  ngOnInit() {
  }

}
