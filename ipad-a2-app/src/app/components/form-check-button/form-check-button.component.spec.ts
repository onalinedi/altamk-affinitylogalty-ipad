import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCheckButtonComponent } from './form-check-button.component';

describe('FormCheckButtonComponent', () => {
  let component: FormCheckButtonComponent;
  let fixture: ComponentFixture<FormCheckButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCheckButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCheckButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
