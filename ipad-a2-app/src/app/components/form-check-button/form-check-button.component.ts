import {Component, OnInit, Input,forwardRef} from '@angular/core';
import {FormControl, ControlValueAccessor,NG_VALUE_ACCESSOR,NG_VALIDATORS} from "@angular/forms";

@Component({
  providers:[
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => FormCheckButtonComponent), multi: true },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => FormCheckButtonComponent), multi: true }
  ],
  selector: 'app-form-check-button',
  templateUrl: 'form-check-button.component.html',
  styleUrls: ['form-check-button.component.scss']
})
export class FormCheckButtonComponent implements OnInit,ControlValueAccessor {
  @Input()
  title: string;
  @Input()
  groupClass: string

  value:any;
  propagateChange:any = () => {};
  validateFn:any = () => {};

  constructor() {
    //console.info("FormCheckButtonComponent: constructor")
  }

  ngOnInit() {
    //console.info("FormCheckButtonComponent: ngoninit")
  }

  writeValue(value: any): void{
    //console.info("FormCheckButtonComponent: writeValue")

    if(value) {
      //console.log("writeValue:" + JSON.stringify(value));
      this.value=value;
    }

  }

  registerOnChange(fn: any): void{
    //console.info("FormCheckButtonComponent: registerOnChange")

    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void{
    //console.info("FormCheckButtonComponent: registerOnTouched")

  }
  validate(c: FormControl) {
    //console.info("FormCheckButtonComponent: validate")

    return null;
  }
}
