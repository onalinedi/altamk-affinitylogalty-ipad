"use strict";
var welcome_component_1 = require('./welcome/welcome.component');
var page_not_found_component_1 = require('./page-not-found/page-not-found.component');
var form1_component_1 = require('./form1/form1.component');
var form2_component_1 = require('./form2/form2.component');
exports.appRoutes = [
    { path: 'welcome', component: welcome_component_1.WelcomeComponent },
    { path: 'form1', component: form1_component_1.Form1Component },
    { path: 'form2', component: form2_component_1.Form2Component },
    { path: '',
        redirectTo: '/welcome',
        pathMatch: 'full'
    },
    { path: '**', component: page_not_found_component_1.PageNotFoundComponent }
];
