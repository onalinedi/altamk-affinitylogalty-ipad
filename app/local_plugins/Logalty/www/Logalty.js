cordova.define("com.altamk.cordova.plugins.logalty.Logalty", function(require, exports, module) {
var exec = require('cordova/exec');

exports.showLogaltyView = function(arg0,arg1, success, error) {
    exec(success, error, "Logalty", "showLogaltyView", [arg0,arg1]);
};

});
