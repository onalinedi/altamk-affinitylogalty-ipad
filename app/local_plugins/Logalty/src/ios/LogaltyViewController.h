//
//  LogaltyViewController.h
//  Affinity Card
//
//  Created by Ivan Rivera on 24/05/2017.
//
//

#ifndef LogaltyViewController_h
#define LogaltyViewController_h
#import "LogaltyBSSClientLibrary.h"
@interface LogaltyViewController : UIViewController <LogaltyBSSViewDelegate>
{
    NSString *url;
    NSString *allowedDomain;
    LogaltyBSSView *logaltyView;
    
}

- (void)configure:(NSString *)url allowedDomain:(NSString *)domain;
- (void)loadURL;
@end


#endif /* LogaltyViewController_h */
