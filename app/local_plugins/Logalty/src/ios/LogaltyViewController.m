//
//  LogaltyViewController.m
//  Affinity Card
//
//  Created by Ivan Rivera on 24/05/2017.
//
//

#import "LogaltyViewController.h"

@interface LogaltyViewController ()

@end

@implementation LogaltyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    [self loadURL];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)configure:(NSString *)url allowedDomain:(NSString *)domain{
    self->url=url;
    self->allowedDomain=domain;
}
- (IBAction)backClicked:(id)sender {
    NSLog(@"Back clicked");
    [self dismissModalViewControllerAnimated:YES];
}
- (IBAction)cancelRequestClicked:(id)sender {
    NSLog(@"Cancel request clicked");
    [self dismissModalViewControllerAnimated:YES];
}
- (IBAction)retryRequestClicked:(id)sender {
    NSLog(@"Retry request clicked");
    [self dismissModalViewControllerAnimated:YES];
}

- (void)loadURL{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSLog(@"Load Url %@",url);
    NSString *trustedUrls = [NSString stringWithFormat:@"%s %s %@", "https://www.logalty.es", "https://www.demo.logalty.es", self->allowedDomain];
    
    [logaltyView setDelegate:self];
    [logaltyView setTrustedURLs:trustedUrls];
    [logaltyView setTransparentSignature:true];
    [logaltyView setPdfNativeViewer:true];
    [logaltyView URLWithString:self->url];
}

-(void)didSignatureCompleted
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Mensaje"
                                                      message:@"Proceso Finalizado"
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}


@end
