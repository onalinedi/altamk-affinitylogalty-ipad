/********* Logalty.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "Logalty.h"
@implementation Logalty

@synthesize childView;

- (void)showLogaltyView:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    printf("\n\n*** LOGALTY VIEW CALLED ***\n\n");
    NSString* url = [command.arguments objectAtIndex:0];
    NSString* allowedDomain = [command.arguments objectAtIndex:1];

    /*if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }*/
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LogaltyStoryboard" bundle:nil];
    LogaltyViewController *vc = [sb instantiateViewControllerWithIdentifier:@"LOGALTY_VIEW"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [vc configure:url allowedDomain:allowedDomain];
    
    __block LogaltyViewController* blockVc = vc;
    __weak Logalty* weakSelf = self;
    
    // Run later to avoid the "took a long time" log message.
    dispatch_async(dispatch_get_main_queue(), ^{
        if (blockVc != nil) {
            CGRect frame = [[UIScreen mainScreen] bounds];
            UIWindow *tmpWindow = [[UIWindow alloc] initWithFrame:frame];
            UIViewController *tmpController = [[UIViewController alloc] init];
            [tmpWindow setRootViewController:tmpController];
            [tmpWindow setWindowLevel:UIWindowLevelNormal];
            
            [tmpWindow makeKeyAndVisible];
            [tmpController presentViewController:blockVc animated:YES completion:nil];
        }
    });
    printf("\n\n*** LOGALTY VIEW CALLED END***\n\n");
}

-(void)createView{
    
}
@end

@interface LogaltyViewController ()
@end

@implementation LogaltyViewController
@synthesize logaltyView;
@synthesize topToolbar;

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    [self loadURL];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configure:(NSString *)url allowedDomain:(NSString *)domain{
    self->url=url;
    self->allowedDomain=domain;
}

- (void)loadURL{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSLog(@"Load Url %@",url);
    NSString *trustedUrls = [NSString stringWithFormat:@"%s %s %@", "https://www.logalty.es", "https://www.demo.logalty.es", self->allowedDomain];
    
    [logaltyView setDelegate:self];
    [logaltyView setTrustedURLs:trustedUrls];
    [logaltyView setTransparentSignature:true];
    [logaltyView setPdfNativeViewer:true];
    [logaltyView URLWithString:self->url];
}

-(void)didSignatureCompleted
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Mensaje"
                                                      message:@"Proceso Finalizado"
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}
- (IBAction)backClicked:(id)sender {
    NSLog(@"Back clicked");
     [self dismissModalViewControllerAnimated:YES];
}
- (IBAction)cancelRequestClicked:(id)sender {
    NSLog(@"Cancel Request clicked");
     [self dismissModalViewControllerAnimated:YES];
}
- (IBAction)retryRequestClicked:(id)sender {
    NSLog(@"Retry Request clicked");
     [self dismissModalViewControllerAnimated:YES];
}
@end
