//
//  Logalty.h
//  Affinity Card
//
//  Created by Ivan Rivera on 22/05/2017.
//
//

#ifndef Logalty_h
#define Logalty_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>
#import "LogaltyBSSClientLibrary.h"

@interface Logalty : CDVPlugin
{
    UIView* childView;
}

@property (nonatomic, retain) UIView* childView;

- (void)createView:(NSString *)url allowedDomain:(NSString *)domain;
@end


@interface LogaltyViewController : UIViewController <LogaltyBSSViewDelegate>
{
    NSString *url;
    NSString *allowedDomain;
}
@property (strong, nonatomic) IBOutlet LogaltyBSSView *logaltyView;
@property (strong, nonatomic) IBOutlet UIToolbar *topToolbar;

- (void)configure:(NSString *)url allowedDomain:(NSString *)domain;
- (void)loadURL;
@end

#endif /* Logalty_h */
