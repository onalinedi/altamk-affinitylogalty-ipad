//
//  LogaltyBSSClientLibrary.h
//  TestLogaltyBSSClientLibrary
//
//  Created by Francisco Soriano on 03/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef TestLogaltyBSSClientLibrary_LogaltyBSSClientLibrary_h
#define TestLogaltyBSSClientLibrary_LogaltyBSSClientLibrary_h

@protocol LogaltyBSSViewDelegate;

@interface LogaltyBSSView : UIView
{
}

@property (strong, nonatomic) NSString *trustedURLs;
@property (readwrite,assign) BOOL pdfNativeViewer;
@property (readwrite,assign) BOOL transparentSignature;
@property (nonatomic, weak) id<LogaltyBSSViewDelegate> delegate;

-(void)URLWithString:(NSString *)url;

@end

@protocol LogaltyBSSViewDelegate <NSObject>

@optional

-(void)didSignatureCompleted;

@end

#endif
