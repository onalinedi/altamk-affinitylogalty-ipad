//
//  CameraOverlayController.h
//  Affinity Card
//
//  Created by Ivan Rivera on 09/07/2017.
//
//

#ifndef CameraOverlayController_h
#define CameraOverlayController_h

#include <UIKit/UIKit.h> 
@interface CameraOverlayController : UIViewController

@end
#endif /* CameraOverlayController_h */
