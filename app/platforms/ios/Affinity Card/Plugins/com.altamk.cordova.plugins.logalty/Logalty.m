/********* Logalty.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "Logalty.h"
#import "CameraOverlayController.h"

@implementation Logalty

@synthesize childView;

- (void)showLogaltyView:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    printf("\n\n*** LOGALTY VIEW CALLED ***\n\n");
    NSString* url = [command.arguments objectAtIndex:0];
    NSString* allowedDomain = [command.arguments objectAtIndex:1];

    /*if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }*/
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LogaltyStoryboard" bundle:nil];
    LogaltyViewController *vc = [sb instantiateViewControllerWithIdentifier:@"LOGALTY_VIEW"];
    vc.cutreListener=self;
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [vc configure:url allowedDomain:allowedDomain];
    
    __block LogaltyViewController* blockVc = vc;
    __weak Logalty* weakSelf = self;
    
    // Run later to avoid the "took a long time" log message.
    dispatch_async(dispatch_get_main_queue(), ^{
        if (blockVc != nil) {
            CGRect frame = [[UIScreen mainScreen] bounds];
            UIWindow *tmpWindow = [[UIWindow alloc] initWithFrame:frame];
            UIViewController *tmpController = [[UIViewController alloc] init];
            [tmpWindow setRootViewController:tmpController];
            [tmpWindow setWindowLevel:UIWindowLevelNormal];
            [tmpWindow makeKeyAndVisible];
            [tmpController presentViewController:blockVc animated:NO completion:nil];
        }
    });
    printf("\n\n*** LOGALTY VIEW CALLED END***\n\n");
}

-(void)takeCameraPhoto:(CDVInvokedUrlCommand*)command{
    CDVPluginResult* pluginResult = nil;
    printf("\n\n*** TAKE CAMERA PHOTO CALLED ***\n\n");
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"CameraOverlay" bundle:nil];
    CameraOverlayController *vc = [sb instantiateViewControllerWithIdentifier:@"OVERLAY_VIEW"];
    vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    __block CameraOverlayController* blockVc = vc;
        
    // Run later to avoid the "took a long time" log message.
    dispatch_async(dispatch_get_main_queue(), ^{
        if (blockVc != nil) {
            CGRect frame = [[UIScreen mainScreen] bounds];
            UIWindow *tmpWindow = [[UIWindow alloc] initWithFrame:frame];
            UIViewController *tmpController = [[UIViewController alloc] init];
            [tmpWindow setRootViewController:tmpController];
            [tmpWindow setWindowLevel:UIWindowLevelNormal];
            [tmpWindow makeKeyAndVisible];
            [tmpController presentViewController:blockVc animated:NO completion:nil];
        }
    });
    printf("\n\n*** LOGALTY VIEW CALLED END***\n\n");
}

-(void)createView{
    
}

-(void)executeJavascript:(NSString*) command{
    [self.commandDelegate evalJs:command];
}
@end

@interface LogaltyViewController ()
@end

@implementation LogaltyViewController
@synthesize logaltyView;
@synthesize topToolbar;

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    [self loadURL];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configure:(NSString *)url allowedDomain:(NSString *)domain{
    self->url=url;
    self->allowedDomain=domain;
}

- (void)loadURL{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSLog(@"Load Url %@",url);
    NSString *trustedUrls = [NSString stringWithFormat:@"%s %s %@", "https://www.logalty.es", "https://www.demo.logalty.es", self->allowedDomain];
    
    [logaltyView setDelegate:self];
    [logaltyView setTrustedURLs:trustedUrls];
    [logaltyView setTransparentSignature:true];
    [logaltyView setPdfNativeViewer:true];
    [logaltyView URLWithString:self->url];
}

-(void)didSignatureCompleted
{
    [self.cutreListener executeJavascript:@"window.logaltyPluginAccess('completedL');"];
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (IBAction)backClicked:(id)sender {
    NSLog(@"Back clicked");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención"
                                                    message:@"Si retrocede se cancelará el proceso de firma, ¿desea continuar?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Retroceder", nil];
    alert.tag=102;
    [alert show];

}
- (IBAction)cancelRequestClicked:(id)sender {
    NSLog(@"Cancel Request clicked");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención"
                                                    message:@"Si cancela la solicitud perderá los datos introducidos, ¿desea continuar?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Si", nil];
    alert.tag=101;
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==101){
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self dismissViewControllerAnimated:NO completion:nil];
            [self.cutreListener executeJavascript:@"window.logaltyPluginAccess('cancelL');"];
            });
                           break;
    }
    }else if(alertView.tag==102){
        switch(buttonIndex) {
            case 0: //"No" pressed
                //do something?
                break;
            case 1: //"Yes" pressed
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self dismissViewControllerAnimated:NO completion:nil];
                [self.cutreListener executeJavascript:@"window.logaltyPluginAccess('backL');"];
                });
                break;
        }
    }else if(alertView.tag==103){
        switch(buttonIndex) {
            case 0: //"No" pressed
                //do something?
                break;
            case 1: //"Yes" pressed
                dispatch_async(dispatch_get_main_queue(), ^(void){
                [self dismissViewControllerAnimated:NO completion:nil];
                [self.cutreListener executeJavascript:@"window.logaltyPluginAccess('retryL');"];
                });
                 break;
        }
    }
}
- (IBAction)retryRequestClicked:(id)sender {
    NSLog(@"Retry Request clicked");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención"
                                                    message:@"¿Desea reiniciar el proceso de firma? Es posible que el servidor esté procesando todavía los datos"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Reiniciar", nil];
    alert.tag=103;
    [alert show];
    }
@end
