//
//  Logalty.h
//  Affinity Card
//
//  Created by Ivan Rivera on 22/05/2017.
//
//

#ifndef Logalty_h
#define Logalty_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>

#import "LogaltyBSSClientLibrary.h"

@protocol CutreListenerProtocol
- (void)executeJavascript:(NSString*) command;
@end

@interface Logalty : CDVPlugin<CutreListenerProtocol>
{
    UIView* childView;
}

@property (nonatomic, retain) UIView* childView;

- (void)createView:(NSString *)url allowedDomain:(NSString *)domain;
@end


@interface LogaltyViewController : UIViewController<UIAlertViewDelegate>
{
    NSString *url;
    NSString *allowedDomain;
    
}
@property (strong, nonatomic) IBOutlet LogaltyBSSView *logaltyView;
@property (strong, nonatomic) IBOutlet UIToolbar *topToolbar;
@property (strong, nonatomic) id<CutreListenerProtocol> cutreListener;
- (void)configure:(NSString *)url allowedDomain:(NSString *)domain;
- (void)loadURL;
@end

#endif /* Logalty_h */
